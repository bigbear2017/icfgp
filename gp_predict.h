#ifndef GP_PREDICT_H__
#define GP_PREDICT_H__

#include <string>
#include "model.h"
#include "matrix.h"
#include "document.h"
namespace psvm {

class GPPredictor {
 public:
  void ReadModel(const char* model_file);                                          // Loads the model from the model_file
  void ReadDocument(const char* train_file, const char* test_file);                // Loads the data from test data file.
  void ReadDocument2(const char* train_file, const char* test_file);                // Loads the data from test data file.
  
  void Predict(const char* model_path, const char* train_file, 
			const char* test_file, const char* mean_res_file_name, const char* variance_res_file_name);
  void Predict2(const char* model_path, const char* train_file, 
			const char* test_file, const char* mean_res_file_name, const char* variance_res_file_name);
  void PredictMean(NormalMatrix * &result);
  void PredictMean2(NormalMatrix * &result);
  void PredictVariance(NormalMatrix *&result);
  void PredictVariance2(NormalMatrix *&result);

  void SaveResult(const char* file_name, NormalMatrix * result, double time );
  void SaveResult2(const char* file_name, NormalMatrix * mean, NormalMatrix * var, double time );
  // Prints the time information of current processor.
  std::string PrintTimeInfo();

  // Save time statistic information into a file. For processor #, it is stored in "path/file_name.#"
  void SaveTimeInfo(const char *path, const char* file_name);

  GPPredictor() {
	  label_ = NULL;
	  covMat_ = NULL;
	  alltCovMat_= NULL;
  }
  ~GPPredictor() {
	  train_doc_.Destroy();
	  test_doc_.Destroy();
	  delete [] label_;
	  if( covMat_ != NULL ) {
	  	delete covMat_;
	  }
	  if( alltCovMat_ != NULL ) {
		  delete alltCovMat_;
	  }
  }

 private:
  // Stores model information including kernel info and support vectors.
  Model model_;
  Document train_doc_;
  Document test_doc_;
  double * label_;
  NormalMatrix* covMat_;
  NormalMatrix* alltCovMat_;
};
}
#endif
