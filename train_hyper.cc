#include <cstring>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <cfloat>
#include "gp_parm.h"
#include "gp_train.h"
#include "timer.h"
#include "common.h"
#include "document.h"
#include "cov.h"
#include "model.h"
#include "matrix.h"
#include "matrix_manipulation.h"
#include "io.h"
#include "util.h"
#include "parallel_interface.h"
#include "train_hyper.h"

using namespace psvm;


//=============================================================================
// Parameter Definitions
HyParam param;
//Document doc;
//ParallelInterface* interface; 
//=============================================================================

void Usage();
void ParseCommandLine(int argc, char** argv);
int main(int argc, char** argv) 
{
	ParallelInterface * interface = ParallelInterface::GetParallelInterface();
	// Initializes the parallel computing environment.
	interface->Init(&argc, &argv);

	ParseCommandLine(argc, argv);               // Parses the commandline options
	TrainingTimeProfile::total.Start();

	HyperTrainer trainer( param );
	trainer.ReadData();
	TrainingTimeProfile::train_model.Start();
	trainer.TrainModel( -100 );
	TrainingTimeProfile::train_model.Stop();
	TrainingTimeProfile::total.Stop(); 
	trainer.SaveTimeInfo();
	// Save time info
	
	interface->Barrier(MPI_COMM_WORLD);
	if (interface->GetProcId() == 0)
		cout << "Saving training time statistic info ... " << endl;

	// Print timing info of processor 0.
	interface->Barrier(MPI_COMM_WORLD);
	if (interface->GetProcId() == 0) 
		cout << endl << trainer.PrintTimeInfo() << endl;

	interface->Finalize();

	return 0;
}
void ParseCommandLine(int argc, char** argv) 
{
	if ( argc < 6 ) {
		Usage();
		exit( 0 );
	}
	
	for(int i=1; i<argc; i += 2) {
		if(i+1 >= argc)
			break;
		if(argv[i][0] != '-')
			break;

		char * param_name = argv[i] + 1;
		char * param_value = argv[i+1];

		if (strcmp(param_name, "f") == 0) 
			param.data_path = string(param_value);
		else if (strcmp(param_name, "h") == 0) 
			param.hyp_path = string(param_value);
		else if (strcmp(param_name, "d") == 0) 
			param.dim = atoi(param_value);
		else if (strcmp(param_name, "t") == 0 )
			param.time_dir = string(param_value);
		else if (strcmp(param_name, "l") == 0 )
			param.log_path = string(param_value);
		else {
			cerr << "Unknown parameter " << param_name << endl;
			Usage();
			exit(2);
		}
	}
}
void Usage() 
{
	const char* msg =
		"usage: ./train_hyper args\r\n"
		"usage: args: -f data_file -h hyp_file -d dim -t time_dir -l log_path\r\n"
		"  Flag descriptions:\n"
		"   -f data_file, the file where input stores \n"
		"   -h hyp_file, the file to store hyper parameters \n"
		"   -d dim, the dimension of input data\n"
		"	-t time_dir, the dir to store traning time info\n"
		"	-l log_file, the file to store log file\n";
	cerr << msg;
}

void HyperTrainer::ReadData() {
	TrainingTimeProfile::read_doc.Start();
	if (_interface->GetProcId() == 0) 
		cout << "Reading data ... " << endl;

	string data_file = _param.data_path;
	if (_interface->GetProcId() == 0) 
		cout << "data file:" << data_file << endl;

	if (!_normalDoc.Read(data_file.c_str())) {
		cerr << "Error reading file: " << data_file << endl;
		_interface->Finalize();
		exit( 3 );
	}

	if (!_allDoc.AllRead(data_file.c_str())) {
		cerr << "Error reading file: " << data_file << endl;
		_interface->Finalize();
		exit( 3 );
	}
	TrainingTimeProfile::read_doc.Stop();

	// Stop timing
}
void HyperTrainer::SaveTimeInfo() {
	ParallelInterface *mpi = ParallelInterface::GetParallelInterface();
	int proc_id   = mpi->GetProcId();

	char filename[4096];                  // Open file file_name.# for writing
	string file_name = "time_info";
	snprintf(filename, sizeof(filename), "%s/%s.%d", param.time_dir.c_str(), file_name.c_str(), proc_id);
	File* obuf = File::OpenOrDie(filename, "w");
	std::string str = PrintTimeInfo();
	CHECK(obuf->WriteString(str) == str.length());
	CHECK(obuf->Flush());
	CHECK(obuf->Close());
	delete obuf;
}

std::string HyperTrainer::PrintTimeInfo() {
	std::string str = "========== Training Time Statistics ==========\n";
	str += " Total                                    : " + TrainingTimeProfile::total.PrintInfo() + "\n";
	str += " 1. Read Document                         : " + TrainingTimeProfile::read_doc.PrintInfo() + "\n";
	str += " 2. Train Model                           : " + TrainingTimeProfile::train_model.PrintInfo() + "\n";
	return str;
}

void HyperTrainer::TrainModel( int max_iter ) {
	const int MAX=20;

	int D = _param.dim;
	int N = _normalDoc.GetGlobalNumberRows();

// Initialize the label values
	double * Y = new double [N];
	_allDoc.GetLocalLabels( Y );

// Initialize the hyper paramaters 
	double * hyper = _hyp;
	double mean = 0.0;                             // mean
	for( int d=0; d<D; d++ ) hyper[d] = 1;  // lengthscale of each dimension
	hyper[D] = 1;                           // signal
	hyper[D+1] = 0.1;                       // noise

	int M;

	double INT   = 0.1;
	double EXT   = 3;
	double SIG   = 0.1;
	double RHO   = SIG / 2;
	double red   = 1.0;
	double RATIO = 10.0;
	
	// learn mean
	mean=average( Y, N );
	
	// learn lengthscale signal noise
	int iter=0;
	int fail=0;

	double x1,x2,x3,x4;
	double F0,f0,f1,f2,f3,f4;
	double d0,d1,d2,d3,d4;

	double* Q = new_VecDoub(D+2);
	double* QQ = new_VecDoub(D+2);
	double* Q0 = new_VecDoub(D+2);
	double* dF0 = new_VecDoub(D+2);
	double* df0 = new_VecDoub(D+2);
	double* df3 = new_VecDoub(D+2);
	double* s = new_VecDoub(D+2);
	
	copy( hyper, Q, D+2 );

	evaluateNlmlGrad( Q, f0, df0 );

	iter = iter + (max_iter<0);

	d0 = 0.0;
	for( int i=0; i<D+2; i++) {
		s[i] = -df0[i];
		d0 += -s[i] * s[i];
	}

	x3 = red / ( 1 - d0 ); // initial search step

	while( iter < abs( max_iter ) ) {
		iter = iter + ( max_iter > 0 );

		// backup
		F0 = f0; 
		copy( Q, Q0, D+2 );
		copy( df0, dF0, D+2 );
		
		M = min( MAX, -max_iter-iter );

		// extrapolation
		while ( 1 ) {
			
			x2 = 0; f2 = f0; d2 = d0; f3 = f0; copy(df0,df3,D+2);
			
			int success = 0;
			while ( success == 0 && M > 0 ) {
				try {
					M = M - 1; iter = iter + ( max_iter < 0 );

					for( int i=0; i<D+2; i++ ) {
						QQ[i] = exp( log(Q[i]) + x3 * s[i] );
					}

					evaluateNlmlGrad( QQ, f3, df3 );
					success = 1;
				}
				catch( exception& e ) {
					x3 = ( x2 + x3 ) / 2;
				}

			}//while success

			if( f3 < F0 ) {
				for( int i=0; i<D+2; i++ ) {
					Q0[i] = exp( log(Q[i]) + x3 * s[i] );
				}
				F0 = f3; copy(df3,dF0,D+2);
			}

			d3 = 0.0;
			for( int i=0; i<D+2; i++ ) {
				d3 += df3[i] * s[i];
			}

			if( d3 > SIG * d0 || f3 > f0 + x3 * RHO * d0 || M == 0 ) break;
			
			x1 = x2; f1 = f2; d1 = d2; // move 2 to 1

			x2 = x3; f2 = f3; d2 = d3; // move 3 to 2

			double A = 6 * ( f1-f2 ) + 3 * ( d2+d1 ) * ( x2-x1 );
			double B = 3 * ( f2-f1 ) - ( 2*d1 + d2 ) * ( x2-x1 );
			x3 = x1 - d1 * ( x2-x1 )*( x2-x1 ) / ( B + sqrt( B*B - A*d1*(x2-x1) ) );
			
			if ( x3 != x3) x3 = x2 * EXT;
			else if ( x3 < 0 ) x3 = x2 * EXT;
			else if ( x3 > x2 * EXT ) x3 = x2 * EXT;
			else if ( x3 < x2 + INT * ( x2-x1 ) ) x3 = x2 + INT * ( x2 - x1 );
		}// while 1 extrapolation

		// interpolation
		while( ( abs(d3) > -SIG * d0 || f3 > f0 + x3*RHO*d0 ) && M > 0 ) {
			if( d3>0 || f3 > f0 + x3 * RHO * d0 ) {
				x4 = x3; f4 = f3; d4 = d3; // move 3 to 4
			}
			else {
				x2 = x3; f2 = f3; d2 = d3; // move 3 to 2
			}

			if( f4 > f0 ) {
				x3 = x2 - ( 0.5 * d2 * ( x4-x2 ) * ( x4-x2) ) / ( f4 - f2 - d2 * ( x4-x2 ) );
			}
			else {
				double A = 6 * ( f2-f4 ) / ( x4-x2 ) + 3 * ( d4+d2 );
				double B = 3 * ( f4-f2 ) - ( 2*d2 + d4 ) * ( x4-x2 );
				x3 = x2 + ( sqrt( B*B - A*d2*(x4-x2) * (x4-x2) ) - B ) / A;
			}

			if ( x3!= x3 ) {
				x3 = ( x2 + x4 ) / 2;
			}

			x3 = max( min( x3, x4-INT*(x4-x2) ), x2 + INT * ( x4-x2 ) );

			for ( int i=0; i<D+2; i++) {
				QQ[i] = exp( log(Q[i]) + x3 * s[i] );
			}

			evaluateNlmlGrad( QQ, f3, df3 );

			if ( f3 < F0 ) {
				for( int i=0; i<D+2; i++ ) {
					Q0[i] = exp( log(Q[i]) + x3 * s[i] );
				}
				F0 = f3; copy(df3,dF0,D+2);
			}
			M--;
			iter = iter + ( max_iter < 0 );

			d3 = 0.0;
			for ( int i=0; i<D+2; i++ ) {
				d3 += df3[i] * s[i];
			}
		}//while interpolation

		if ( abs(d3) < -SIG * d0 && f3 < f0 + x3 * RHO * d0 ) {
			for ( int i=0; i<D+2; i++ ) {
				Q[i] = exp( log(Q[i]) + x3 * s[i] );
			}

			f0 = f3;

			if( _interface->GetProcId() == 0 ) {
				printf("iter: %3d\t\tnlml = %lf\r\n",iter,f0);

				FILE* fid = fopen(_param.log_path.c_str(),"a");
				fprintf(fid,"iter: %3d\t\tnlml = %lf\r\n",iter,f0);
				fprintf(fid,"mean = %lf\r\n",mean);
				for(int d=0;d<D;d++)fprintf(fid,"lengthscale %d = %lf\r\n",d,Q[d]);
				fprintf(fid,"sig = %lf\r\n",Q[D]);
				fprintf(fid,"niose = %lf\r\n",Q[D+1]);
				fclose(fid);
			}

			double temp = 0.0;
			double df32 = 0.0, df30 = 0.0, df00 = 0.0;
			for ( int i=0; i<D+2; i++ ) {
				df32 += df3[i] * df3[i];
				df30 += df0[i] * df3[i];
				df00 += df0[i] * df0[i];
			}
			temp = ( df32 - df30 ) / df00;

			for ( int i=0; i<D+2; i++ ) {
				s[i] = temp * s[i] - df3[i];
			}

			for ( int i=0; i<D+2; i++ ) {
				df0[i] = df3[i];
			}

			d3 = d0;

			d0 = 0.0;
			for ( int i=0; i<D+2; i++) {
				d0 += df0[i] * s[i];
			}

			if ( d0 > 0 ) {
				for ( int i=0; i<D+2; i++ ) {
					s[i] = -df0[i];
				}
				d0 = 0.0;
				for ( int i=0; i<D+2; i++) {
					d0 += -s[i] * s[i];
				}
			}

			x3 = x3 * min( RATIO, d3 / ( d0 - DBL_MIN ) );
			fail = 0;

		}
		else {
			for ( int i=0; i<D+2; i++ ) {
				Q[i] = Q0[i];
			}
			f0 = F0;
			for ( int i=0; i<D+2; i++ ) {
				df0[i] = dF0[i];
			}
			if ( fail == 1 || iter > abs( max_iter ) ) {
				break;
			}

			for ( int i=0; i<D+2; i++ ) {
				s[i] = -df0[i];
			}
			d0 = 0.0;
			for ( int i=0; i<D+2; i++ ) {
				d0 += -s[i] * s[i];
			}
			x3 = 1 / ( 1 - d0 );
			fail = 1;
		}//if

	}//while iter

	for(int i=0; i<D+2; i++ ) {
		hyper[i] = Q[i];
	}

	if( _interface->GetProcId() == 0 ) {
		FILE* fid = fopen(_param.log_path.c_str(),"a");
		fprintf(fid,"final step nlml=%lf\r\n",f0);
		fprintf(fid,"mean = %lf\r\n ",mean);
		for(int d=0;d<D;d++)fprintf(fid,"lengthscale %d = %lf\r\n",d,Q[d]);
		fprintf(fid,"sig = %lf\r\n",Q[D]);
		fprintf(fid,"niose = %lf\r\n",Q[D+1]);
		fclose(fid);
	}

	del_VecDoub(Q,D+2);
	del_VecDoub(QQ,D+2);
	del_VecDoub(Q0,D+2);
	del_VecDoub(dF0,D+2);
	del_VecDoub(df0,D+2);
	del_VecDoub(df3,D+2);
	del_VecDoub(s,D+2);
}

void HyperTrainer::evaluateNlmlGrad( double * hyp , double & f, double * df ){
//hyp[0:dim-1] are length scales, hyp[dim] is signal variance, hyp[dim+1] 
//is noise variance
//--------------------------------------------------------------//
//-------------------Marginal Loglikelihood---------------------//
//--------------------------------------------------------------//
	int numGlobal = _normalDoc.GetGlobalNumberRows();
	int numLocal = _normalDoc.GetLocalNumberRows();
	int dim = _param.dim;
	int myid = _interface->GetProcId();
	int numProcs = _interface->GetNumProcs();
	double * ell = new double [ dim ];
	for( int i = 0; i < dim; i++ ) {
		ell[i] = hyp[i];
	}
	ParallelMatrix * cf = new ParallelMatrix( numGlobal, numGlobal );
	COV * covNoise = new CovSENoise( hyp[dim], hyp[dim+1], dim , ell );

	//int * pivot = new int[ numGlobal ];
	//MatrixManipulation::ICF( _normalDoc, *covNoise, numGlobal, numGlobal, 0, cf, pivot );
	MatrixManipulation::CF( _allDoc, *covNoise, numGlobal, numGlobal, 0, cf );
	ParallelMatrix * cft = new ParallelMatrix();
	MatrixManipulation::ParallelTranspose( *cf, cft);
	delete covNoise;

	//double logdet = MatrixManipulation::ParallelLogDet( *cf, pivot );
	double logdet = MatrixManipulation::ParallelLogDet( *cf );
	//cout << "logdet " << logdet << endl;
//--------------------------------------------------------------//
	//COV * cov2 = new CovSEISO( hyp[dim], dim, ell );

	double * y = new double [numGlobal];
	_allDoc.GetLocalLabels( y );
	//double * yc = new double[ numGlobal ];
	//for( int i = 0; i < numGlobal; i++ ) {
	//	yc[i] = y[pivot[i]];
	//}
	double* tky = new double [numGlobal];
	double* kiy = new double [numGlobal];
	for( int i = 0; i < numGlobal; i++ ) {
		tky[i] = 0;
		kiy[i] = 0;
	}
	MatrixManipulation::CholForwardSub( *cf, y, tky );
	MatrixManipulation::CholBackwardSubT( *cft, tky, kiy ); 
	
	delete [] tky;
	double yky = 0;
	for( int i = 0; i < numGlobal; i++ ) {
		yky += kiy[i] * y[i];
	}
	f = 0.5 * ( logdet + yky );
	
//-------------------------------------------------------------//
//--------------------Gradient Part----------------------------//
//-------------------------------------------------------------//
	ParallelMatrix * invk = new ParallelMatrix( numGlobal, numGlobal );

	/*
	double* eyei = new double [ numGlobal ];
	for( int j = 0; j < numGlobal; j++ ) {
		eyei[ j ] = 0;
	}
	double * tempe = new double[ numGlobal ];
	double * tempr = new double[ numGlobal ];
	for( int i = 0; i < numGlobal; i++ ) {
		eyei[i] = 1;
		for( int j = 0; j < numGlobal; j++ ) {
			tempe[j] = 0;
			tempr[j] = 0;
		}
		MatrixManipulation::CholForwardSub( *cf, eyei, tempe );
		MatrixManipulation::CholBackwardSubT( *cft, tempe, tempr);
		if( i % numProcs == myid ) {
			int localIndex = _interface->ComputeGlobalToLocal(i, myid );
			for( int j = 0; j < numGlobal; j++ ) {
				invk->Set( localIndex, j, tempr[j] );
			}
		}
		eyei[i] = 0;
	}
	*/

	int num_start = numProcs < 10? 2*numProcs : numProcs;
	int num_end = num_start + 10;
	int num_split = num_start;
	for( int ns = num_start; ns < num_end; ns++ ) {
		if( numGlobal % ns  == 0 ){
			num_split = ns;
			break;
		}
	}
	if( num_split == num_start && numGlobal % num_split != 0 ) {
		cout << "Can't split the data properly. " << endl
			<<  "The number of training cases need be divided by a num between" 
			<< num_start << " and " << num_end << endl;
		exit( 0 );
	}
	int num_cols = numGlobal / num_split;
	NormalMatrix * eyei = new NormalMatrix( numGlobal, num_cols);
	NormalMatrix * tempe = new NormalMatrix( numGlobal, num_cols);
	NormalMatrix * tempr = new NormalMatrix( numGlobal, num_cols);
	for( int i = 0; i < num_split; i++ ) {
		for( int j = 0; j < numGlobal; j++ ) {
			for( int k = 0; k < num_cols; k++ ) {
				eyei->Set( j, k, 0 );
				tempe->Set(j, k, 0 );
				tempr->Set(j, k, 0 );
			}
		}
		int num_shift = i * num_cols;
		for( int j = 0; j < num_cols; j++ ) {
			eyei->Set( j + num_shift, j, 1 );
		}
		MatrixManipulation::CholForwardSub( *cf, *eyei, tempe );
		MatrixManipulation::CholBackwardSubT( *cft, *tempe, tempr);
		for( int c = i * num_cols; c < (i+1) * num_cols; c++ ) {
			if( c % numProcs == myid ) {
				int localIndex = _interface->ComputeGlobalToLocal(c, myid );
				for( int j = 0; j < numGlobal; j++ ) {
					invk->Set( localIndex, j, tempr->Get(j, c - num_shift) );
				}
			}
		}
	}
	/*
	   if( myid == 0 ) {
	   for( int i = 0; i < numLocal; i++ ) {
	   for( int j = 0; j < numGlobal; j++ ) {
	   cout << cf->Get( i, j ) << " ";
	   }
	   cout << endl;
	   }
	   }
	   */
	delete tempe, delete tempr, delete eyei;
	delete cf, delete cft;

	//calculate the parallel matrix invk - alpha on the fly
	for( int i = 0; i < numGlobal; i++ ) {
		if( i % numProcs == myid ) {
			int localIndex = _interface->ComputeGlobalToLocal( i, myid );
			for( int j = 0; j < numGlobal; j++ ) {
				double temp_value = invk->Get(localIndex, j) - kiy[i] * kiy[j];
				invk->Set( localIndex, j, temp_value );
			}
		}
	}
	delete [] kiy;

	ParallelMatrix* dvMatrix = new ParallelMatrix( numGlobal, numGlobal );
	for( int index = 0; index < dim+2; index++ ) {
		COV * covDiv = new CovSEDiv( hyp[dim], hyp[dim+1], dim, ell, index );
		for( int i = 0; i < numLocal; i++ ) {
			const Sample & a = *_normalDoc.GetLocalSample( i );
			for( int j = 0; j < numGlobal; j++ ) {
				const Sample & b = *(_allDoc.GetLocalSample( j ));
				double v = covDiv->CalcCov( a, b );
				dvMatrix->Set( i, j, v );
			}
		}
		double gd = 0;
		MatrixManipulation::PmPmTrace( *invk, *dvMatrix, gd );
		df[ index ] = 0.5 * gd;
		delete covDiv;
	}
	delete dvMatrix, delete invk;
}
HyperTrainer::HyperTrainer( HyParam & param ) {
	_param = param;
	_interface = ParallelInterface::GetParallelInterface();
	_numProcs = _interface->GetNumProcs(); //records the num of processes
	_hyp = new_VecDoub ( param.dim + 2 );
}
