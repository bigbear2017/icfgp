#include <float.h>
#include <cmath>
#include "matrix_manipulation.h"
#include "io.h"
#include "util.h"
#include "matrix.h"
#include "parallel_interface.h"
#include "document.h"
#include "cov.h"

namespace psvm {

// Cholesky factorization: factorize matrix A into LL^T. "Original" represents A, low_triangular represents L.
// The memory block of L is allocated in this function, which will be freed by calling Destory() (which is done in the destructor of LLMatrix).
bool MatrixManipulation::CF(const LLMatrix &original, LLMatrix *low_triangular) {
  CHECK(low_triangular);
  int dim = original.GetDim();
  low_triangular->Init(dim);
  for (int i = 0; i < dim; ++i) {
    for (int j = i; j < dim; ++j) {
      double sum = original.Get(j, i);
      for (int k = i-1; k >= 0; --k) {
        sum -= low_triangular->Get(i, k) * low_triangular->Get(j, k);
      }
      if (i == j) {
        if (sum <= 0) {  // sum should be larger than 0
          cerr << "Only symmetric positive definite matrix can perform Cholesky factorization.";
          return false;
        }
        low_triangular->Set(i, i, sqrt(sum));
      } else {
        low_triangular->Set(j, i, sum/low_triangular->Get(i, i));
      }
    }
  }
  return true;
}

// For the processor storing the pivot sample in local machine (here, 'doc'),
// *pivot_sample will be assigned to be the pointer to the pivot sample.
// For other processors, pivot sample data will be packed and broadcasted from
// the processor owning it, and a Sample '**pivot_sample' will be allocated to
// store pivot sample data.
//
// 'pivot_global_index' is used to verify whether the pivot sample is stored in current processor (in 'doc').
void MatrixManipulation::BroadcastPivotSample(const Document &doc, int pivot_global_index, Sample **pivot_sample) {
  CHECK(pivot_sample);
  ParallelInterface *mpi = ParallelInterface::GetParallelInterface();
  int parallel_id = mpi->GetProcId();
  int pnum = mpi->GetNumProcs();

  int pivot_local_index = mpi->ComputeGlobalToLocal(pivot_global_index, parallel_id);

  int buff_size = 0;
  char *buffer = NULL;
  if (pivot_local_index != -1) {
    // Pack the pivot sample data
    *pivot_sample = const_cast<Sample*>(doc.GetLocalSample(pivot_local_index));
    buff_size = Document::PackSample(buffer, **pivot_sample);

    // Broadcast package size
    mpi->Bcast(&buff_size, 1, MPI_INT, parallel_id, MPI_COMM_WORLD);

    // Broadcast package
    mpi->Bcast(buffer, buff_size, MPI_BYTE, parallel_id, MPI_COMM_WORLD);
  } else {
    int root = pivot_global_index % pnum;
    // Broadcast package size
    mpi->Bcast(&buff_size, 1, MPI_INT, root, MPI_COMM_WORLD);

    // Prepare a suitable memory block to receive package.
    buffer = new char[buff_size];

    // Broadcast package
    mpi->Bcast(buffer, buff_size, MPI_BYTE, root, MPI_COMM_WORLD);

    Document::UnpackSample(*pivot_sample, buffer);
  }
  delete [] buffer;
}

void MatrixManipulation::FreePivotSample(Sample *pivot_sample, int pivot_index) {
  ParallelInterface *mpi = ParallelInterface::GetParallelInterface();
  int parallel_id = mpi->GetProcId();
  if (mpi->ComputeGlobalToLocal(pivot_index, parallel_id) == -1) {
    delete pivot_sample;
  }
}

void MatrixManipulation::ICF(const Document &doc, const COV& cov, int rows, int columns, double threshold, ParallelMatrix *icf) {
  CHECK(icf);
  ParallelInterface *mpi = ParallelInterface::GetParallelInterface();
  int parallel_id = mpi->GetProcId();
  int pnum = mpi->GetNumProcs();
  icf->Init(rows, columns);
  int local_rows = mpi->ComputeNumLocal(rows);
  int* pivot = new int[columns];
  bool* pivot_selected = new bool[local_rows];

  // diag1: the diagonal part of K (the kernal matrix diagonal)
  // diag2: the quadratic sum of a row of the ICF matrix
  double* diag1 = new double[local_rows];
  double* diag2 = new double[local_rows];
  for (int i = 0; i < local_rows; ++i) {
    diag1[i] = cov.CalcCov(*doc.GetLocalSample(i), *doc.GetLocalSample(i));
    diag2[i] = 0;
    pivot_selected[i] = 0;
  }


  cout << "What's the kernel matrix" << endl;
	for(int i=0; i<local_rows; ++i)
	{
		for(int j=0; j<local_rows; ++j)
		{
			//cout << cov.CalcCov(*doc.GetLocalSample(i), *doc.GetLocalSample(j)) << " ";
			//if( j % 10 == 9 ) {
			//	cout << endl;
			//}
		}
		//cout << endl;
	}


  double* header_row = new double[columns];
  for (int column = 0; column < columns; column++) {
    // Get global trace
    double local_trace = 0;
    for (int i = 0; i < local_rows; i++) {
      // If pivot_selected[i] == true, diag1[i] - diag2[i] == 0, summation is not needed.
      if (pivot_selected[i] == false) 
		  local_trace += diag1[i] - diag2[i];
    }
    double global_trace = DBL_MAX;
    mpi->AllReduce(&local_trace, &global_trace, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    // Test stop criterion
    if (global_trace < threshold) {
      icf->SetNumCols(column);
      if (parallel_id == 0) 
        cout << "reset columns from " << columns << " to " << icf->GetNumCols();
      break;
    }

    // Find local pivot
    Pivot local_pivot;
    local_pivot.pivot_value = -DBL_MAX;
    local_pivot.pivot_index = -1;
    for (int i = 0; i < local_rows; ++i) {
      double tmp = diag1[i] - diag2[i];
      if (pivot_selected[i] == false && tmp > local_pivot.pivot_value) {
        local_pivot.pivot_index = mpi->ComputeLocalToGlobal(i, parallel_id);
        local_pivot.pivot_value = tmp;
      }
    }

    // Get global pivot (MPI_Reduce is used)
    Pivot global_pivot;
    mpi->AllReduce(&local_pivot, &global_pivot, 1, MPI_DOUBLE_INT, MPI_MAXLOC, MPI_COMM_WORLD);

    // Update pivot vector
    pivot[column] = global_pivot.pivot_index;

    // Broadcast pivot sample
    Sample *pivot_sample = NULL;
    BroadcastPivotSample(doc, global_pivot.pivot_index, &pivot_sample);

    // Broadcast the row corresponds to pivot.
    int header_row_size = column + 1;
    int localRowID = mpi->ComputeGlobalToLocal(global_pivot.pivot_index, parallel_id);
    if (localRowID != -1) {
      icf->Set(localRowID, column, sqrt(global_pivot.pivot_value));
      for (int j = 0; j < header_row_size; ++j) {
        header_row[j] = icf->Get(localRowID, j);
      }

      mpi->Bcast(header_row, header_row_size,
                 MPI_DOUBLE, parallel_id, MPI_COMM_WORLD);

      // Update pivot flag vector
      pivot_selected[localRowID] = true;
    } else {
      int root = global_pivot.pivot_index % pnum;
      mpi->Bcast(header_row, header_row_size, MPI_DOUBLE, root, MPI_COMM_WORLD);
    }

    // Calculate the column'th column
    // Note: 1. This order can improve numerical accuracy.
    //       2. Cache is used, will be faster too.
    for (int i = 0; i < local_rows; ++i) {
      if (pivot_selected[i] == false) {
        icf->Set(i, column, 0);
      }
    }
    for (int k = 0; k < column; ++k) {
      for (int i = 0; i < local_rows; ++i) {
        if (pivot_selected[i] == false) {
          icf->Set(i, column, icf->Get(i, column) -
                          icf->Get(i, k) * header_row[k]);
        }
      }
    }
    for (int i = 0; i < local_rows; ++i) {
      if (pivot_selected[i] == false) {
        icf->Set(i, column, icf->Get(i, column) +
          cov.CalcCov(*doc.GetLocalSample(i), *pivot_sample));
      }
    }
    for (int i = 0; i < local_rows; ++i) {
      if (pivot_selected[i] == false) {
        icf->Set(i, column, icf->Get(i, column)/header_row[column]);
      }
    }

    // Free pivot sample
    FreePivotSample(pivot_sample, global_pivot.pivot_index);

    // Update diagonal
    for (int i = 0; i < local_rows; ++i) {
      diag2[i] += icf->Get(i, column) * icf->Get(i, column);
    }
  }
  delete[] pivot;
  delete[] pivot_selected;
  delete[] diag1;
  delete[] diag2;
  delete[] header_row;
}


void MatrixManipulation::ICF(const Document &doc, const COV& cov, int rows, int columns, double threshold, ParallelMatrix *icf, int * pivot) {
  CHECK(icf);
  ParallelInterface *mpi = ParallelInterface::GetParallelInterface();
  int parallel_id = mpi->GetProcId();
  int pnum = mpi->GetNumProcs();
  icf->Init(rows, columns);
  int local_rows = mpi->ComputeNumLocal(rows);
  //int* pivot = new int[columns];
  bool* pivot_selected = new bool[local_rows];

  // diag1: the diagonal part of K (the kernal matrix diagonal)
  // diag2: the quadratic sum of a row of the ICF matrix
  double* diag1 = new double[local_rows];
  double* diag2 = new double[local_rows];
  for (int i = 0; i < local_rows; ++i) {
    diag1[i] = cov.CalcCov(*doc.GetLocalSample(i), *doc.GetLocalSample(i));
    diag2[i] = 0;
    pivot_selected[i] = 0;
  }

  double* header_row = new double[columns];
  for (int column = 0; column < columns; column++) {
    // Get global trace
    double local_trace = 0;
    for (int i = 0; i < local_rows; i++) {
      // If pivot_selected[i] == true, diag1[i] - diag2[i] == 0, summation is not needed.
      if (pivot_selected[i] == false) 
		  local_trace += diag1[i] - diag2[i];
    }
    double global_trace = DBL_MAX;
    mpi->AllReduce(&local_trace, &global_trace, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    // Test stop criterion
    if (global_trace < threshold) {
      icf->SetNumCols(column);
      if (parallel_id == 0) 
        cout << "reset columns from " << columns << " to " << icf->GetNumCols();
      break;
    }

    // Find local pivot
    Pivot local_pivot;
    local_pivot.pivot_value = -DBL_MAX;
    local_pivot.pivot_index = -1;
    for (int i = 0; i < local_rows; ++i) {
      double tmp = diag1[i] - diag2[i];
      if (pivot_selected[i] == false && tmp > local_pivot.pivot_value) {
        local_pivot.pivot_index = mpi->ComputeLocalToGlobal(i, parallel_id);
        local_pivot.pivot_value = tmp;
      }
    }

    // Get global pivot (MPI_Reduce is used)
    Pivot global_pivot;
    mpi->AllReduce(&local_pivot, &global_pivot, 1, MPI_DOUBLE_INT, MPI_MAXLOC, MPI_COMM_WORLD);

    // Update pivot vector
    pivot[column] = global_pivot.pivot_index;

    // Broadcast pivot sample
    Sample *pivot_sample = NULL;
    BroadcastPivotSample(doc, global_pivot.pivot_index, &pivot_sample);

    // Broadcast the row corresponds to pivot.
    int header_row_size = column + 1;
    int localRowID = mpi->ComputeGlobalToLocal(global_pivot.pivot_index, parallel_id);
    if (localRowID != -1) {
      icf->Set(localRowID, column, sqrt(global_pivot.pivot_value));
      for (int j = 0; j < header_row_size; ++j) {
        header_row[j] = icf->Get(localRowID, j);
      }

      mpi->Bcast(header_row, header_row_size,
                 MPI_DOUBLE, parallel_id, MPI_COMM_WORLD);

      // Update pivot flag vector
      pivot_selected[localRowID] = true;
    } else {
      int root = global_pivot.pivot_index % pnum;
      mpi->Bcast(header_row, header_row_size, MPI_DOUBLE, root, MPI_COMM_WORLD);
    }

    // Calculate the column'th column
    // Note: 1. This order can improve numerical accuracy.
    //       2. Cache is used, will be faster too.
    for (int i = 0; i < local_rows; ++i) {
      if (pivot_selected[i] == false) {
        icf->Set(i, column, 0);
      }
    }
    for (int k = 0; k < column; ++k) {
      for (int i = 0; i < local_rows; ++i) {
        if (pivot_selected[i] == false) {
          icf->Set(i, column, icf->Get(i, column) -
                          icf->Get(i, k) * header_row[k]);
        }
      }
    }
    for (int i = 0; i < local_rows; ++i) {
      if (pivot_selected[i] == false) {
        icf->Set(i, column, icf->Get(i, column) +
          cov.CalcCov(*doc.GetLocalSample(i), *pivot_sample));
      }
    }
    for (int i = 0; i < local_rows; ++i) {
      if (pivot_selected[i] == false) {
        icf->Set(i, column, icf->Get(i, column)/header_row[column]);
      }
    }

    // Free pivot sample
    FreePivotSample(pivot_sample, global_pivot.pivot_index);

    // Update diagonal
    for (int i = 0; i < local_rows; ++i) {
      diag2[i] += icf->Get(i, column) * icf->Get(i, column);
    }
  }
  delete[] pivot_selected;
  delete[] diag1;
  delete[] diag2;
  delete[] header_row;
}

void MatrixManipulation::CF( const Document &allDoc, const COV &cov, int row, int columns, double threshold, ParallelMatrix * cf ){
	//doesn't depend on the number of machines, if the number of points can not
	//be devided by the number of machines, that's fine
	ParallelInterface *mpi = ParallelInterface::GetParallelInterface();
  	int myid = mpi->GetProcId();
	int numProcs = mpi->GetNumProcs();
	int numRows = cf->GetNumRows();
	int numCols = cf->GetNumCols();
	if( numRows!= row || numCols != columns ) {
		cf->Init( row, columns );
	}
	int numLocal = cf->GetNumLocalRows();
  	double* diag1 = new double[row];
	//double* diag2 = new double[row];
  	for (int i = 0; i < row; ++i) {
		if( i % numProcs == myid ) {
    		diag1[i] = cov.CalcCov(*allDoc.GetLocalSample(i), *allDoc.GetLocalSample(i));
		} else {
			diag1[i] = 0;
		}
		//diag2[i] = 0;
  	}
	for( int col = 0; col < columns; col++ ) {
		double value = 0;
		double* header_row = new double [ col + 1 ];
		int header_row_size = col + 1;
		if( col % numProcs == myid ) {
			int localRow = mpi->ComputeGlobalToLocal( col, myid );
			value = sqrt( diag1[col] );
			cf->Set( localRow, col, value );
			for( int h = 0; h < header_row_size; h++ ) {
				header_row[h] = cf->Get( localRow, h );
			}
			mpi->Bcast( header_row, header_row_size, MPI_DOUBLE, myid, MPI_COMM_WORLD );
		}else {
			mpi->Bcast( header_row, header_row_size, MPI_DOUBLE, col % numProcs, MPI_COMM_WORLD );
		}
		for( int j = col+1; j < row; j++ ) {
			if( j % numProcs == myid ) {
				int localRow = mpi->ComputeGlobalToLocal( j, myid );
				double kij = cov.CalcCov( *allDoc.GetLocalSample(col), *allDoc.GetLocalSample(j) );
				for( int h= 0; h < col; h++ ) {
					kij -= cf->Get( localRow, h ) * header_row[h];
				}
				double cij = kij / header_row[col];
				cf->Set( localRow, col, cij );
				diag1[j] -= cij * cij;
			}
		}
		delete [] header_row;
	}
	delete [] diag1;
}
void MatrixManipulation::ProductMM(const ParallelMatrix &icf, const double *diagonal, LLMatrix *product) 
{
  CHECK(product);
  int row = icf.GetNumLocalRows();
  int column = icf.GetNumCols();
  double* buff = new double[max(row, (column + 1) * column / 2)];
  double* result = new double[(column + 1) * column / 2];
  int offset = 0;
  for (int i = 0; i < column; ++i) {
    offset += i;
    for (int p = 0; p < row; ++p) {
      buff[p] = icf.Get(p, i) * diagonal[p];
    }
    for (int j = 0; j <= i; ++j) {
      double tmp = 0;
      for (int p = 0; p < row; ++p) {
        tmp += buff[p] * icf.Get(p, j);
      }
      result[offset+j] = tmp;
    }
  }
  ParallelInterface *mpi = ParallelInterface::GetParallelInterface();
  mpi->Reduce(result, buff, (column + 1) * column / 2, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  if (mpi->GetProcId() == 0) {
    int disp = 0;
    for (int i = 0; i < column; ++i) {
      for (int j = 0; j <= i; ++j) {
        product->Set(i, j, buff[disp++] + (i == j ? 1 : 0));
      }
    }
  }
  delete[] buff;
  delete[] result;
}

void MatrixManipulation::ProductAllMM(const ParallelMatrix &icf, const double *diagonal, LLMatrix *product) 
{
  CHECK(product);
  int row = icf.GetNumLocalRows();
  int column = icf.GetNumCols();
  double* buff = new double[max(row, (column + 1) * column / 2)];
  double* result = new double[(column + 1) * column / 2];
  int offset = 0;
  for (int i = 0; i < column; ++i) {
    offset += i;
    for (int p = 0; p < row; ++p) {
      buff[p] = icf.Get(p, i) * diagonal[p];
    }
    for (int j = 0; j <= i; ++j) {
      double tmp = 0;
      for (int p = 0; p < row; ++p) {
        tmp += buff[p] * icf.Get(p, j);
      }
      result[offset+j] = tmp;
    }
  }
  ParallelInterface *mpi = ParallelInterface::GetParallelInterface();
  mpi->AllReduce(result, buff, (column + 1) * column / 2, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  int disp = 0;
  for (int i = 0; i < column; ++i) {
    for (int j = 0; j <= i; ++j) {
      product->Set(i, j, buff[disp++] + (i == j ? 1 : 0));
    }
  }
  delete[] buff;
  delete[] result;
}
void MatrixManipulation::PlPlProduct(const ParallelMatrix &aMat, const ParallelMatrix &bMat, NormalMatrix *product) {
	CHECK(product);
	int aRow = aMat.GetNumLocalRows();
	int aCol = aMat.GetNumCols();
	int bRow = bMat.GetNumLocalRows();
	int bCol = bMat.GetNumCols();
	CHECK(aRow == bRow);             //Check whether matrix mulplication can be applied to aMat and bMat
	product->SetSize(aCol, bCol);
	double* buf = new double[aCol * bCol];
	double* result = new double[aCol * bCol];
	int offset = 0;
	for(int i=0; i<aCol; i++) {
		for(int j=0; j<bCol; j++) {
			double tmp = 0;
			for(int k=0; k<aRow; k++) 
				tmp += aMat.Get(k, i) * bMat.Get(k, j);
			buf[offset++] = tmp;
		}
	}
	ParallelInterface *mpi = ParallelInterface::GetParallelInterface();
	mpi->Reduce(buf, result, offset, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	if(mpi->GetProcId() == 0) {
		int disp = 0;
		for(int i=0; i<aCol; i++) {
			for(int j=0; j<bCol; j++) {
				product->Set(i, j, result[disp++]);
			}
		}
	}
	delete[] buf;
	delete[] result;
}
void MatrixManipulation::PlNmProduct(const ParallelMatrix &aMat, const NormalMatrix &bMat, ParallelMatrix *product)
{
	CHECK(product);
	int aRow = aMat.GetNumRows();
	int aCol = aMat.GetNumCols();
	int bRow = bMat.GetNumRows();
	int bCol = bMat.GetNumCols();
	CHECK(aCol == bRow);
	product->Init(aRow, bCol);
	int aLocalRow = aMat.GetNumLocalRows();
	for(int i=0; i<aLocalRow; i++) {
		for(int j=0; j<bCol; j++) {
			double tmp = 0;
			for(int k=0; k<aCol; k++)
				tmp += aMat.Get(i, k) * bMat.Get(k, j);
			product->Set(i, j, tmp);
		}
	}
}
void MatrixManipulation::TplNmProduct( const ParallelMatrix &aMat, const NormalMatrix &bMat, NormalMatrix * cMat ) {

	int aRow = aMat.GetNumRows();
	int aCol = aMat.GetNumCols();
	int aLoc = aMat.GetNumLocalRows();
	int bRow = bMat.GetNumRows();
	int bCol = bMat.GetNumCols();


	NormalMatrix  tbMat (bCol, bRow);
	for( int i = 0; i < bCol; i++ ) {
		for( int j = 0; j < bRow; j++ ) {
			tbMat.Set( i, j, bMat.Get( j, i ) );
		}
	}
	
	CHECK( aRow == bRow );
	cMat->SetSize( aCol, bCol );
	int mSize = aCol * bCol;

	ParallelInterface * mpi= ParallelInterface::GetParallelInterface();
	int myid = mpi->GetProcId();
	int startInd = mpi->ComputeStartInd( aRow );
	PredictingTimeProfile::calc.Start();
	double* res  = new double [ mSize ];
	double* rArr = new double [ mSize ];
	int offset = 0;
	for( int i = 0; i < aCol; i++ ) {
		for( int j = 0; j < bCol; j++ ) {
			double tmp = 0;
			for( int k = 0; k < aLoc; k++ ) {
				//tmp += aMat.Get( k, i ) * bMat.Get( startInd + k, j );
				tmp += aMat.Get( k, i ) * tbMat.Get( j, startInd + k );
			}
			rArr[ offset++ ] = tmp;
		}
	}
	PredictingTimeProfile::calc.Stop();
	//cout << "aMat 0,0 : " << aMat.Get( 0, 0 ) << endl;
	//cout << "ystart : " << bMat.Get( startInd, 0 ) << endl;
	mpi->Reduce( rArr, res, offset, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	if( myid == 0) {
		int disp = 0;
		for(int i=0; i<aCol; i++) {
			for(int j=0; j<bCol; j++) {
				cMat->Set(i, j, res[disp++]);
				//cout << cMat->Get( i, j ) << endl;
			}
		}
	}
	delete [] res, delete [] rArr;
}


void MatrixManipulation::NmTplProduct( const NormalMatrix &aMat, const ParallelMatrix &bMat, NormalMatrix * cMat ) {
	int aRow = aMat.GetNumRows();
	int aCol = aMat.GetNumCols();
	int bRow = bMat.GetNumRows();
	int bCol = bMat.GetNumCols();
	int bLoc = bMat.GetNumLocalRows();
	
	CHECK( aCol == bCol );

	cMat->SetSize( aRow, bRow );
	for( int i = 0; i < aRow; i++ ) {
		for( int j = 0; j < bRow; j++ ) {
			cMat->Set( i, j, 0 );
		}
	}

	ParallelInterface * mpi= ParallelInterface::GetParallelInterface();
	int startInd = mpi->ComputeStartInd( bRow );
	//double* res  = new double [aRow];
	for( int i = 0; i < aRow; i++ ) {
		for( int j = 0; j < bLoc; j++ ) {
			double tmp = 0;
			for( int k = 0; k < aCol; k++ ) {
				tmp += aMat.Get( i, k ) * bMat.Get( j, k );
			}
			cMat->Set( i, startInd+j, tmp );
		}
	}
	int buf_size = aRow * bRow;
	double * buf = new double[buf_size];
	double * result = new double[buf_size];
	NormalMatrix::PackSample( buf, cMat, aRow, bRow );
  	mpi->Reduce( buf, result, buf_size, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	NormalMatrix::UnpackSample( cMat, result, aRow, bRow);
	delete[] buf, delete [] result;
}
void MatrixManipulation::PlNmProduct( const ParallelMatrix &aMat, const NormalMatrix &bMat, NormalMatrix * cMat ) {
	int aRow = aMat.GetNumRows();
	int aCol = aMat.GetNumCols();
	int aLoc = aMat.GetNumLocalRows();
	int bRow = bMat.GetNumRows();
	int bCol = bMat.GetNumCols();

	CHECK( aCol == bRow );

	cMat->SetSize( aRow, bCol );
	for( int i = 0; i < aRow; i++ ) {
		for( int j = 0; j < bCol; j++ ) {
			cMat->Set( i, j, 0 );
		}
	}
	ParallelInterface * mpi= ParallelInterface::GetParallelInterface();
	int startInd = mpi->ComputeStartInd( aRow );
	for( int i = 0; i < aLoc; i++ ) {
		for( int j = 0; j < bCol; j++ ) {
			double tmp = 0;
			for( int k = 0; k < aCol; k++ ) {
				tmp += aMat.Get( i, k ) * bMat.Get( k, j );
			}
			cMat->Set( startInd + i, j, tmp );
		}
	}
	int buf_size = aRow * bCol;
	double * buf = new double [buf_size];
	double * result = new double[buf_size];
	NormalMatrix::PackSample(buf, cMat, aRow, bCol);
  	mpi->Reduce( buf, result, buf_size, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	NormalMatrix::UnpackSample( cMat, result, aRow, bCol);
	delete[] buf, delete[] result;
}

void MatrixManipulation::NmNmProduct( const NormalMatrix &aMat, const NormalMatrix &bMat, NormalMatrix * cMat ) {
	int aRow = aMat.GetNumRows();
	int aCol = aMat.GetNumCols();
	int bRow = bMat.GetNumRows();
	int bCol = bMat.GetNumCols();

	CHECK( aCol == bRow );

	int cRow = cMat->GetNumRows();
	int cCol = cMat->GetNumCols();
	if( aRow != cRow || bCol != cCol ) {
		cMat->SetSize( aRow, bCol );
	}
	for( int i = 0; i < aRow; i++ ) {
		for( int j = 0; j < bCol; j++ ) {
			double tmp = 0;
			for( int k = 0; k < aCol; k++ ) {
				tmp += aMat.Get( i, k ) * bMat.Get( k, j );
			}
			cMat->Set( i, j, tmp );
		}
	}
}


void MatrixManipulation::NmNmtProduct( const NormalMatrix &aMat, const NormalMatrix &bMat, NormalMatrix * cMat ) {
	int aRow = aMat.GetNumRows();
	int aCol = aMat.GetNumCols();
	int bRow = bMat.GetNumRows();
	int bCol = bMat.GetNumCols();

	CHECK( aCol == bCol );

	int cRow = cMat->GetNumRows();
	int cCol = cMat->GetNumCols();
	if( aRow != cRow || bRow != cCol ) {
		cMat->SetSize( aRow, bRow );
	}
	for( int i = 0; i < aRow; i++ ) {
		for( int j = 0; j < bRow; j++ ) {
			double tmp = 0;
			for( int k = 0; k < aCol; k++ ) {
				tmp += aMat.Get( i, k ) * bMat.Get( j, k );
			}
			cMat->Set( i, j, tmp );
		}
	}
}

double MatrixManipulation::ParallelLogDet( const ParallelMatrix & pm, const int * pivot ) {
	ParallelInterface *mpi = ParallelInterface::GetParallelInterface();
	int numCores = mpi->GetNumProcs();
	int procId = mpi->GetProcId();
	int numRows = pm.GetNumRows();
	int localRows = pm.GetNumLocalRows();
	double * buff = new double[ numRows ] ;
	for( int i = 0; i < numRows; i++ ) {
		buff[i] = 0;
	}
	for( int i = 0; i < numRows; i++ ) {
		int index = pivot[i];
		if( index % numCores == procId ) {
			int r = mpi->ComputeGlobalToLocal( index, procId );
			int c = i;
			buff[i] = pm.Get( r, c );
		}
	}

	double * res = new double[ numRows ];
  	mpi->AllReduce( buff, res, numRows, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	double logdet = 0;
	for( int i = 0; i < numRows; i++ ) {
		logdet += 2 * log( res[i] );
	}
	return logdet;

}

double MatrixManipulation::ParallelLogDet( const ParallelMatrix & pm ) {
	ParallelInterface *mpi = ParallelInterface::GetParallelInterface();
	int numCores = mpi->GetNumProcs();
	int procId = mpi->GetProcId();
	int numRows = pm.GetNumRows();
	int localRows = pm.GetNumLocalRows();
	double * buff = new double[ numRows ] ;
	for( int i = 0; i < numRows; i++ ) {
		buff[i] = 0;
	}
	for( int i = 0; i < numRows; i++ ) {
		if( i % numCores == procId ) {
			int r = mpi->ComputeGlobalToLocal( i, procId );
			int c = i;
			buff[i] = pm.Get( r, c );
		}
	}

	double * res = new double[ numRows ];
  	mpi->AllReduce( buff, res, numRows, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	double logdet = 0;
	for( int i = 0; i < numRows; i++ ) {
		logdet += 2 * log( res[i] );
	}
	return logdet;
}

void MatrixManipulation::LLMatrixInverse( const LLMatrix & m, NormalMatrix * r ) {
	int rows = m.GetDim();
	if( r->GetNumRows() != rows || r->GetNumRows() != rows ) {
		r->SetSize( rows, rows );
	}

	NormalMatrix * idt = NormalMatrix::getIdenty( rows );
	LLMatrix* temp = new LLMatrix();
	NormalMatrix * HX = new NormalMatrix(); 
	
	MatrixManipulation::CF( m, temp );
	MatrixManipulation::CholForwardSub( *temp, idt , HX );
	MatrixManipulation::CholBackwardSub( *temp, HX, r );   //the result of sollving HH'X = B is stored in product
	delete idt, delete temp, delete HX;

}
void MatrixManipulation::CholBackwardSub(const LLMatrix &a, const double *b,
		double *x) {
	int dim = a.GetDim();
	for (int k = dim - 1; k >= 0; --k) {
		double tmp = b[k];
		for (int i = k + 1; i < dim; ++i) {
			tmp -= x[i] * a.Get(i, k);
		}
		x[k] = tmp / a.Get(k, k);
	}
}
void MatrixManipulation::CholBackwardSub(const LLMatrix &a, const NormalMatrix *b, NormalMatrix *x)
{
	int aDim = a.GetDim();
	int bRow = b->GetNumRows();
	int bCol = b->GetNumCols();
	CHECK(aDim == bRow);
	x->SetSize(bRow, bCol);
	double * bVec = new double[bRow];
	double * xVec = new double[bRow];
	for(int j=0; j<bCol; j++) 
	{
		for(int i=0; i<bRow; i++) 
			bVec[i] = b->Get(i, j);
		CholBackwardSub(a, bVec, xVec);
		for(int i=0; i<bRow; i++)
			x->Set(i, j, xVec[i]);
	}
	delete[] bVec;
	delete[] xVec;
}

void MatrixManipulation::CholForwardSub(const LLMatrix &a, const double *b, double *x) {
	int dim = a.GetDim();
	for (int k = 0; k < dim; ++k) {
		double tmp = b[k];
		for (int i = 0; i < k; ++i) {
			tmp -= x[i] * a.Get(k, i);
		}
		x[k] = tmp / a.Get(k, k);
	}
}

void MatrixManipulation::CholForwardSub(const LLMatrix &a, const NormalMatrix *b, NormalMatrix *x)
{
	int aDim = a.GetDim();
	int bRow = b->GetNumRows();
	int bCol = b->GetNumCols();
	CHECK(aDim == bRow);
	x->SetSize(bRow, bCol);
	double * bVec = new double[bRow];
	double * xVec = new double[bRow];
	for(int j=0; j<bCol; j++) {
		for(int i=0; i<bRow; i++) 
			bVec[i] = b->Get(i, j);
		CholForwardSub(a, bVec, xVec);
		for(int i=0; i<bRow; i++)
			x->Set(i, j, xVec[i]);
	}
	delete[] bVec;
	delete[] xVec; 
}

void MatrixManipulation::CholForwardSub(const ParallelMatrix& a, const double * b, double * x, const int* pivot ) {
	ParallelInterface * interface = ParallelInterface::GetParallelInterface();
	int myid = interface->GetProcId();
	int numProcs = interface->GetNumProcs();
	int numGlobal = a.GetNumRows();
	double* temp = new double [ numGlobal ];
	for( int i = 0; i < numGlobal; i++ ) {
		temp[i] = b[i];
	}
	for( int row = 0; row < numGlobal; row++ ) {
		int index = pivot[row];
		if( index % numProcs == myid ) {
			int localIndex = interface->ComputeGlobalToLocal( index, myid );
			x[row] = temp[row]/a.Get( localIndex, row);
			interface->Bcast( &x[row], 1, MPI_DOUBLE, myid, MPI_COMM_WORLD );
		}else{
			interface->Bcast( &x[row], 1, MPI_DOUBLE, index%numProcs, MPI_COMM_WORLD );
		}
		for( int i = row+1; i < numGlobal; i++ ) {
			int index = pivot[ i ];
			if( index % numProcs == myid ) {
				int localIndex = interface->ComputeGlobalToLocal( index , myid );
				temp[i] -= x[row] * a.Get( localIndex, row );
			}
		}
	}
	delete [] temp;
}


void MatrixManipulation::CholForwardSub(const ParallelMatrix& a, const double * b, double * x) {
	ParallelInterface * interface = ParallelInterface::GetParallelInterface();
	int myid = interface->GetProcId();
	int numProcs = interface->GetNumProcs();
	int numGlobal = a.GetNumRows();
	double* temp = new double [ numGlobal ];
	for( int i = 0; i < numGlobal; i++ ) {
		temp[i] = b[i];
	}
	for( int row = 0; row < numGlobal; row++ ) {
		if( row % numProcs == myid ) {
			int localIndex = interface->ComputeGlobalToLocal( row, myid );
			x[row] = temp[row]/a.Get( localIndex, row);
			interface->Bcast( &x[row], 1, MPI_DOUBLE, myid, MPI_COMM_WORLD );
		}else{
			interface->Bcast( &x[row], 1, MPI_DOUBLE, row%numProcs, MPI_COMM_WORLD );
		}
		for( int i = row+1; i < numGlobal; i++ ) {
			//int index = pivot[ i ];
			if( i % numProcs == myid ) {
				int localIndex = interface->ComputeGlobalToLocal( i, myid );
				temp[i] -= x[row] * a.Get( localIndex, row );
			}
		}
	}
	delete [] temp;
}

void MatrixManipulation::CholForwardSub( const ParallelMatrix & a, NormalMatrix &b, NormalMatrix * x ) {
	ParallelInterface * interface = ParallelInterface::GetParallelInterface();
	int myid = interface->GetProcId();
	int numProcs = interface->GetNumProcs();
	int numGlobal = a.GetNumRows();
	int numCols = x->GetNumCols();
	for( int row = 0; row < numGlobal; row++ ) {
		if( row % numProcs == myid ) {
			int localIndex = interface->ComputeGlobalToLocal( row, myid );
			for( int i = 0; i < numCols; i++ ) {
				x->Set( row, i, b.Get( row, i ) / a.Get( localIndex, row ) );
			}
			interface->Bcast( (*x)[row], numCols, MPI_DOUBLE, myid, MPI_COMM_WORLD );
		}else{
			interface->Bcast( (*x)[row], numCols, MPI_DOUBLE, row%numProcs, MPI_COMM_WORLD );
		}
		for( int i = row+1; i < numGlobal; i++ ) {
			//int index = pivot[ i ];
			if( i % numProcs == myid ) {
				int localIndex = interface->ComputeGlobalToLocal( i, myid );
				for( int j = 0; j < numCols; j++ ) {
					double temp_value = b.Get(i, j) - x->Get( row, j ) * a.Get( localIndex, row );
					b.Set( i, j, temp_value );
				}
			}
		}
	}
}

void MatrixManipulation::CholBackwardSubT( const ParallelMatrix& a, NormalMatrix &b, NormalMatrix * x ) {
	ParallelInterface * interface = ParallelInterface::GetParallelInterface();
	int myid = interface->GetProcId();
	int numProcs = interface->GetNumProcs();
	int numGlobal = a.GetNumRows();
	int numCols = x->GetNumCols();
	for( int column = numGlobal - 1; column>= 0; column-- ) {
		int index = column;
		if( index % numProcs == myid ) {
			int localIndex = interface->ComputeGlobalToLocal( index, myid );
			for( int i = 0; i < numCols; i++ ) {
				x->Set( column, i, b.Get( column, i ) / a.Get( localIndex, column ) );
			}
			interface->Bcast( (*x)[column], numCols, MPI_DOUBLE, myid, MPI_COMM_WORLD );
		} else {
			interface->Bcast( (*x)[column], numCols, MPI_DOUBLE, index % numProcs, MPI_COMM_WORLD );
		}
		for( int i = 0; i < column; i++ ) {
			int index = i;
			if( index % numProcs == myid ) {
				int localIndex = interface->ComputeGlobalToLocal( index, myid );
				for( int j = 0; j < numCols; j++ ) {
					double temp_value = b.Get(i, j) - x->Get( column, j ) * a.Get( localIndex, column);
					b.Set( i, j, temp_value );
				}
			}
		}
	}
}

void MatrixManipulation::CholBackwardSub(const ParallelMatrix& a, const double * b, double * x ) {
	ParallelInterface * interface = ParallelInterface::GetParallelInterface();
	int myid = interface->GetProcId();
	int numProcs = interface->GetNumProcs();
	int numGlobal = a.GetNumRows();
	double * temp = new double[ numGlobal ];
	for( int i = 0; i < numGlobal; i++ ) {
		temp[i] = b[i];
	}
	for( int column = numGlobal - 1; column>= 0; column-- ) {
		if( column % numProcs == myid ) {
			int localIndex = interface->ComputeGlobalToLocal( column, myid );
			x[column] = temp[column] / a.Get( localIndex, column );
			interface->Bcast( &x[column], 1, MPI_DOUBLE, myid, MPI_COMM_WORLD );
		} else {
			interface->Bcast( &x[column], 1, MPI_DOUBLE, column % numProcs, MPI_COMM_WORLD );
		}

		double * buff = new double [ column ];
		if( column % numProcs == myid ) {
			int localIndex = interface->ComputeGlobalToLocal( column, myid );
			for( int i = 0; i < column; i++ ) {
				buff[ i ] = -x[column] * a.Get( localIndex, i );
			}
			interface->Bcast( buff, column, MPI_DOUBLE, myid, MPI_COMM_WORLD );
		}
		else {
			interface->Bcast( buff, column, MPI_DOUBLE, column % numProcs, MPI_COMM_WORLD );
		}
		for( int i = 0; i < column; i++ ) {
			temp[i] -= buff[i];
		}
		delete [] buff;
	}
	delete [] temp;
}

void MatrixManipulation::CholBackwardSubT( const ParallelMatrix& a, const double * b, double * x ) {
	ParallelInterface * interface = ParallelInterface::GetParallelInterface();
	int myid = interface->GetProcId();
	int numProcs = interface->GetNumProcs();
	int numGlobal = a.GetNumRows();
	double * temp = new double[ numGlobal ];
	for( int i = 0; i < numGlobal; i++ ) {
		temp[i] = b[i];
	}
	for( int column = numGlobal - 1; column>= 0; column-- ) {
		int index = column;
		if( index % numProcs == myid ) {
			int localIndex = interface->ComputeGlobalToLocal( index, myid );
			x[column] = temp[column] / a.Get( localIndex, column );
			interface->Bcast( &x[column], 1, MPI_DOUBLE, myid, MPI_COMM_WORLD );
		} else {
			interface->Bcast( &x[column], 1, MPI_DOUBLE, index % numProcs, MPI_COMM_WORLD );
		}
		for( int i = 0; i < column; i++ ) {
			int index = i;
			if( index % numProcs == myid ) {
				int localIndex = interface->ComputeGlobalToLocal( index, myid );
				temp[i] -= x[column] * a.Get( localIndex, column );
			}
		}
	}
	delete [] temp;
}

void MatrixManipulation::ParallelTranspose( const ParallelMatrix & a, ParallelMatrix * b ) {
	int numRows = a.GetNumRows();
	int numCols = a.GetNumCols();

	ParallelInterface * mpi = ParallelInterface::GetParallelInterface();
	int numProcs = mpi->GetNumProcs();
	int myid = mpi->GetProcId();
	b->Init( numCols, numRows );
	double * temp = new double[ numRows ];
	double * res  = new double[ numRows ];
	for( int col = 0; col < numCols; col++ ) {
		//for each row in the new parallel matrix
		int tid = col % numProcs;
		//where we should put the row

		for( int i = 0; i < numRows; i++ ) {
			//each row size is numRows
			int rid = i % numProcs;
			//collect the distributed row from all machines
			if( rid == myid ) {
				int localIndex = mpi->ComputeGlobalToLocal( i, myid );
				temp[i] = a.Get( localIndex, col );
			} else {
				temp[i] = 0;
			}
		}
		//sum them together
		mpi->Reduce( temp, res, numRows, MPI_DOUBLE, MPI_SUM, tid, MPI_COMM_WORLD );
		if( tid == myid ) {
			//copy the values to the matrix
			int localIndex = mpi->ComputeGlobalToLocal( col, myid );
			for( int i = 0; i < numRows; i++ ) {
				b->Set( localIndex, i, res[i] );
			}
		}
	}
	delete [] temp;
	delete [] res;
}


  void MatrixManipulation::ParallelTranspose( const ParallelMatrix & a, ParallelMatrix * b, const int *pivot ) {
	int numRows = a.GetNumRows();
	int numCols = a.GetNumCols();

	ParallelInterface * mpi = ParallelInterface::GetParallelInterface();
	int numProcs = mpi->GetNumProcs();
	int myid = mpi->GetProcId();
	b->Init( numCols, numRows );
	double * temp = new double[ numRows ];
	double * res  = new double[ numRows ];
	for( int col = 0; col < numCols; col++ ) {
		//for each row in the new parallel matrix
		int tid = col % numProcs;
		//where we should put the row

		for( int i = 0; i < numRows; i++ ) {
			//each row size is numRows
			int rid = pivot[i];
			//collect the distributed row from all machines
			if( rid % numProcs == myid ) {
				int localIndex = mpi->ComputeGlobalToLocal( pivot[i], myid );
				temp[i] = a.Get( localIndex, col );
			} else {
				temp[i] = 0;
			}
		}
		//sum them together
		mpi->Reduce( temp, res, numRows, MPI_DOUBLE, MPI_SUM, tid, MPI_COMM_WORLD );
		if( tid == myid ) {
			//copy the values to the matrix
			int localIndex = mpi->ComputeGlobalToLocal( col, myid );
			for( int i = 0; i < numRows; i++ ) {
				b->Set( localIndex, i, res[i] );
			}
		}
	}
	delete [] temp;
	delete [] res;
  }

//This method will tranpose a parallel Lower trangular matrix into an upper triangular matrix
void MatrixManipulation::ParallelLLTranspose( const ParallelMatrix & a, ParallelMatrix * b ) {
	int numRows = a.GetNumRows();
	int numCols = a.GetNumCols();

	ParallelInterface * mpi = ParallelInterface::GetParallelInterface();
	int numProcs = mpi->GetNumProcs();
	int myid = mpi->GetProcId();
	b->Init( numCols, numRows );
	cout << "init finished" << endl;
	cout << "numRows: " << numRows << "numCols: " << numCols << endl;

	for( int col = 0; col < numCols; col++ ) {
		//for each row in new parallel matrix
		int tid = col % numProcs;
		int rowSize = numRows - col;
		//the first row size is numRows, then reduce one by one
		double * temp = new double[ rowSize ];
		double * res  = new double[ rowSize ];

		//copy the distributed row from all machines
		for( int i = col; i < numRows; i++ ) {
			int rid = i % numProcs;
			if( rid == myid ) {
				int localIndex = mpi->ComputeGlobalToLocal( i, myid );
				temp[i] = a.Get( localIndex, col );
			} else {
				temp[i] = 0;
			}
		}

		//sum to the machine which will store this row
		mpi->Reduce( temp, res, rowSize, MPI_DOUBLE, MPI_SUM, tid, MPI_COMM_WORLD );
		if( tid == myid ) {
			int localIndex = mpi->ComputeGlobalToLocal( col, myid );
			//copy the value to the matrix
			for( int i = 0; i < numRows; i++ ) {
				if( i < col ) {
					b->Set( localIndex, i, 0 );
				} else {
					b->Set( localIndex, i, res[i-col] );
				}
			}
		}
		delete [] temp;
		delete [] res;
	}
}

void MatrixManipulation::PmPmTrace( const ParallelMatrix & a, const ParallelMatrix & b, double & trace ){
	int numLocal = a.GetNumLocalRows();
	int numLocalb = b.GetNumLocalRows();
	CHECK( numLocal == numLocalb );
	
	int numCols = a.GetNumCols();
	double sum = 0;
	for( int i = 0; i < numLocal; i++ ) {
		for( int j = 0; j < numCols; j++ ) {
			sum += a.Get( i, j) * b.Get( i, j );
		}
	}
	ParallelInterface* mpi = ParallelInterface::GetParallelInterface();
	mpi->AllReduce( &sum, &trace, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD );
}
}  // namespace psvm
