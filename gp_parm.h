#ifndef GP_PARM_H__
#define GP_PARM_H__
#include <string>
namespace psvm {
struct GPParameter 
{
  double rank_ratio;                 // ICF factorization resulted $p/n$
  double threshold;                  // ICF threshold
  double * ell;
  double sf;
  double sigma;
  int dim;
  int nob;
  char model_path[4096];
};
struct HyParam{
	std::string data_path;
	std::string hyp_path;
	int dim;
	int unum;
	//int train_num;
	//int total_num;
	std::string time_dir;
	std::string log_path;
	std::string u_path;
};
}

#endif
