#include <cstring>
#include <string>
#include <cstdlib>
#include <cstdio>
#include "gp_parm.h"
#include "gp_train.h"
#include "timer.h"
#include "common.h"
#include "document.h"
#include "cov.h"
#include "model.h"
#include "matrix.h"
#include "matrix_manipulation.h"
#include "io.h"
#include "util.h"
#include "parallel_interface.h"

namespace psvm {
	void GPTrainer::TrainModel(const Document& doc, const GPParameter& parameter, const COV& cov, Model* model) 
	{
		TrainingTimeProfile::icf.Start();
		ParallelInterface *mpi = ParallelInterface::GetParallelInterface();
		ParallelMatrix * icf_result = new ParallelMatrix();

		if (mpi->GetProcId() == 0) {
			cout << "Performing ICF ... " << endl;
			cout << cov.get_parm_str() << endl;
		}
		Timer t1;
		t1.Start();
		int rank = static_cast<int>(doc.GetGlobalNumberRows() * parameter.rank_ratio);
		if( mpi->GetProcId() == 0 ) 
			cout << "rank is : " << rank << endl;
		model->set_rank(rank);
		MatrixManipulation::ICF(doc, cov, doc.GetGlobalNumberRows(), rank, parameter.threshold, icf_result);
		t1.Stop();
		if (mpi->GetProcId() == 0) 
			cout << "ICF completed in " << t1.total() << " seconds" << endl;

		//icf_result->Save(".", "icf_mm");
		//  icf_result.Load(".", "icf");
		TrainingTimeProfile::icf.Stop();

		//Get the model
		TrainingTimeProfile::production.Start();
		rank = icf_result->GetNumCols();
		model->set_rank(rank);

		LLMatrix * cf = new LLMatrix(rank);
		int row_cnt = doc.GetGlobalNumberRows();
		double * diagonal = new double[row_cnt];
		for(int i=0; i<row_cnt; i++)
		{
			diagonal[i] = 1.0 /( model->get_sigma() * model->get_sigma());
			//	cout << diagonal[i] << "	";
		}
		//  cout << endl;

		MatrixManipulation::ProductMM(*icf_result, diagonal, cf);

		//  cout << "The result of the product is:" << endl;
		//cf->Show();

		delete [] diagonal;

		if( mpi->GetProcId() == 0 ) {
			TrainingTimeProfile::cf.Start();
			LLMatrix * cf_res = new LLMatrix();
			if(!MatrixManipulation::CF(*cf, cf_res))
			{
				cerr << "Failed when doing Complete Cholesky Factorization!" << endl;
				exit(1);
			}
			cout << "CF end !" << endl;
			model->set_cf_res(cf_res);
			TrainingTimeProfile::cf.Stop();
		}
		mpi->Barrier( MPI_COMM_WORLD );
		model->set_icf_res(icf_result);
		TrainingTimeProfile::production.Stop();

		model->set_trtime( TrainingTimeProfile::icf.total() + TrainingTimeProfile::production.total() );
		//  cout << "The complete cholesky factorization is :" << endl;
		//  cf_res->Show();

		//Save the model
		TrainingTimeProfile::store_model.Start();
		model->Save(parameter.model_path, "model");

		//  cout << "Save end!" << endl;
		//  cout << "train end!" << endl;
		TrainingTimeProfile::store_model.Stop();

		delete cf;
		//  cout << "After delte the model!" << endl;
	}
}
using namespace psvm;

//=============================================================================
// Parameter Definitions

// Incomplete Cholesky Fatorization related options
double FLAGS_fact_threshold = 1.0e-5;
double FLAGS_rank_ratio = 1.0;

// Kernel function related options
int FLAGS_cov_type = 0;
double FLAGS_sf = -1;
double FLAGS_ell = -1;

double FLAGS_sigma = 0.01;
double FLAGS_mean  = 0.00;
string HP_file = "";

// Where to save the result
string FLAGS_model_path = ".";

//=============================================================================

void Usage();
void ParseCommandLine(int argc, char** argv);
int main(int argc, char** argv) 
{
	// Initializes the parallel computing environment.
	ParallelInterface* interface = ParallelInterface::GetParallelInterface();
	interface->Init(&argc, &argv);

	ParseCommandLine(argc, argv);               // Parses the commandline options

	GPTrainer trainer;
	Model model;
	Document doc;
	GPParameter parameter;
	COV * cov = new CovSEISO( HP_file );
	FLAGS_sigma = cov->getSigma();
	FLAGS_mean  = cov->getMean();

	// Sets the commandline options
	parameter.rank_ratio = FLAGS_rank_ratio;
	parameter.threshold = FLAGS_fact_threshold;
	strncpy(parameter.model_path, FLAGS_model_path.c_str(), sizeof(parameter.model_path));
	model.set_cov(cov);

	TrainingTimeProfile::total.Start();

	TrainingTimeProfile::read_doc.Start();
	if (interface->GetProcId() == 0) 
		cout << "Reading data ... " << endl;

	string data_file = argv[1];
	if (interface->GetProcId() == 0) 
		cout << "data file:" << data_file << endl;
	if (!doc.Read(data_file.c_str())) {
		cerr << "Error reading file: " << data_file << endl;
		interface->Finalize();
		return 3;
	}
	if (interface->GetProcId() == 0) 
		TrainingTimeProfile::read_doc.Stop();

	TrainingTimeProfile::train_model.Start();
	model.set_sigma(FLAGS_sigma);
	model.set_mean (FLAGS_mean );
	trainer.TrainModel(doc, parameter, (*cov), &model);
	TrainingTimeProfile::train_model.Stop();
	TrainingTimeProfile::total.Stop();                  // Stop timing

	// Save time info
	interface->Barrier(MPI_COMM_WORLD);
	if (interface->GetProcId() == 0) 
		cout << "Saving training time statistic info ... " << endl;

	trainer.SaveTimeInfo(parameter.model_path, "TrainingTimeInfo");
	// Print timing info of processor 0.
	interface->Barrier(MPI_COMM_WORLD);
	if (interface->GetProcId() == 0) 
		cout << endl << trainer.PrintTimeInfo() << endl;

	interface->Finalize();
	delete cov;
	return 0;
}
void ParseCommandLine(int argc, char** argv) 
{
	if ( argc < 2 ) {
		Usage();
		exit( 0 );
	}
	for(int i=2; i<argc; i += 2) {
		if(i+1 >= argc)
			break;
		if(argv[i][0] != '-')
			break;

		char * param_name = argv[i] + 1;
		char * param_value = argv[i+1];

		if (strcmp(param_name, "ft") == 0) 
			FLAGS_fact_threshold = atof(param_value);
		else if (strcmp(param_name, "rr") == 0) 
			FLAGS_rank_ratio = atof(param_value);
		else if (strcmp(param_name, "mp") == 0) 
			FLAGS_model_path = string(param_value);
		else if (strcmp(param_name, "hp") == 0 )
			HP_file = string(param_value);
		else {
			cerr << "Unknown parameter " << param_name << endl;
			Usage();
			exit(2);
		}
	}
}
void Usage() 
{
	const char* msg =
		"Usage: gp_train data_file\n"
		"  Flag descriptions:\n"
		"    -ft(fact_threshold) (When to stop ICF. Should be in (0, 1]).  type: double.  default: 1.0000000000000001e-05\n"
		"    -rr(rank_ratio)					type: double \n"
		"    -hp(hyper paramter file)			type:string \n"
		"    -mp(model_path) (Where to save the resulting model) type: string default: .\n";
	cerr << msg;
}

void GPTrainer::SaveTimeInfo(const char * path, const char * file_name) {
	ParallelInterface *mpi = ParallelInterface::GetParallelInterface();
	int proc_id   = mpi->GetProcId();

	char filename[4096];                  // Open file file_name.# for writing
	snprintf(filename, sizeof(filename), "%s/%s.%d", path, file_name, proc_id);
	File* obuf = File::OpenOrDie(filename, "w");
	std::string str = PrintTimeInfo();
	CHECK(obuf->WriteString(str) == str.length());
	CHECK(obuf->Flush());
	CHECK(obuf->Close());
	delete obuf;
}

std::string GPTrainer::PrintTimeInfo() {
	std::string str = "========== Training Time Statistics ==========\n";
	str += " Total                                    : " + TrainingTimeProfile::total.PrintInfo() + "\n";
	str += " 1. Read Document                         : " + TrainingTimeProfile::read_doc.PrintInfo() + "\n";
	str += " 2. Train Model                           : " + TrainingTimeProfile::train_model.PrintInfo() + "\n";
	str += "    2.1 ICF                               : " + TrainingTimeProfile::icf.PrintInfo() + "\n";
	str += "    2.2 Production                        : " + TrainingTimeProfile::production.PrintInfo() + "\n";
	str += "    2.3 CF                                : " + TrainingTimeProfile::cf.PrintInfo() + "\n";
	str += " 3. Store Model                           : " + TrainingTimeProfile::store_model.PrintInfo() + "\n";
	return str;
}

