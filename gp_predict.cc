#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <utility>
#include <string>
#include <vector>
#include "gp_predict.h"
#include "document.h"
#include "matrix_manipulation.h"
#include "timer.h"
#include "util.h"
#include "io.h"
#include "parallel_interface.h"

namespace psvm {
	void GPPredictor::ReadModel(const char* model_file) {
		model_.Load(model_file, "model");
	}

	void GPPredictor::ReadDocument(const char* train_file_name, const char* test_file_name) {
		train_doc_.Read(train_file_name);
		test_doc_.AllRead(test_file_name);
		label_ = new double [test_doc_.GetGlobalNumberRows()];
		test_doc_.GetLocalLabels(label_);
	}
	void GPPredictor::ReadDocument2(const char* train_file_name, const char* test_file_name) {
		train_doc_.AllRead(train_file_name);
		test_doc_.Read(test_file_name);
		Document test_label;
		test_label.AllRead( test_file_name );
		label_ = new double [test_label.GetGlobalNumberRows()];
		test_label.GetLocalLabels(label_);

		const COV * cov = model_.get_cov();
		int localTestRows = test_doc_.GetLocalNumberRows();
		int globalTestRows = test_doc_.GetGlobalNumberRows();
		int globalTrainRows = train_doc_.GetGlobalNumberRows();
		covMat_ = new NormalMatrix( localTestRows, globalTrainRows );
		alltCovMat_ = new NormalMatrix( globalTrainRows, globalTestRows);

		for( int i = 0; i < localTestRows; i++ ) {
			for( int j = 0; j < globalTrainRows; j++ ) {
				covMat_->Set( i, j, cov->CalcCov(*train_doc_.GetLocalSample(j), *test_doc_.GetLocalSample(i)) );
			}
		}

		for( int i = 0; i < globalTrainRows; i++ ) {
			for( int j = 0; j < globalTestRows; j++ ) {
				alltCovMat_->Set( i, j, cov->CalcCov(*train_doc_.GetLocalSample(i), *test_label.GetLocalSample(j)) );
			}
		}
	}
	void GPPredictor::Predict(const char* model_path, const char* train_file, 
			const char* test_file, const char* mean_res_file_name, const char* variance_res_file_name) {
		PredictingTimeProfile::read_model.Start();
		ReadModel(model_path);
		PredictingTimeProfile::read_model.Stop();

		PredictingTimeProfile::read_doc.Start();
		ReadDocument(train_file, test_file);
		PredictingTimeProfile::read_doc.Stop();

		ParallelInterface* interface = ParallelInterface::GetParallelInterface();
		interface->Barrier(MPI_COMM_WORLD);
		if ( interface->GetProcId() == 0 ) {
			cout << "File Loading End" << endl;
		}
		NormalMatrix * mean = NULL;
		NormalMatrix * variance = NULL;


		PredictingTimeProfile::premean.Start();
		PredictMean(mean);
		PredictingTimeProfile::premean.Stop();
		PredictingTimeProfile::prevar.Start();
		PredictVariance(variance);
		PredictingTimeProfile::prevar.Stop();
		double total_time = model_.get_trtime() + PredictingTimeProfile::premean.total()
			+ PredictingTimeProfile::prevar.total();

		ParallelInterface* mpi = ParallelInterface::GetParallelInterface();
		if( mpi->GetProcId() == 0 ) {
			SaveResult2(mean_res_file_name, mean, variance, total_time);
		}
		delete mean;
		delete variance;
	}

	void GPPredictor::Predict2(const char* model_path, const char* train_file, 
			const char* test_file, const char* mean_res_file_name, const char* variance_res_file_name) {
		PredictingTimeProfile::read_model.Start();
		ReadModel(model_path);
		PredictingTimeProfile::read_model.Stop();

		PredictingTimeProfile::read_doc.Start();
		ReadDocument2(train_file, test_file);
		PredictingTimeProfile::read_doc.Stop();
		NormalMatrix * mean = NULL;
		NormalMatrix * variance = NULL;


		PredictingTimeProfile::premean.Start();
		PredictMean2(mean);
		PredictingTimeProfile::premean.Stop();
		PredictingTimeProfile::prevar.Start();
		PredictVariance2(variance);
		PredictingTimeProfile::prevar.Stop();
		double total_time = model_.get_trtime() + PredictingTimeProfile::premean.total()
			+ PredictingTimeProfile::prevar.total();

		ParallelInterface* mpi = ParallelInterface::GetParallelInterface();
		if( mpi->GetProcId() == 0 ) {
			SaveResult2(mean_res_file_name, mean, variance, total_time);
		}
		delete mean;
		delete variance;
	}
	void GPPredictor::SaveResult(const char* file_name, NormalMatrix * result, double time = 0)
	{
		File* obuf = File::OpenOrDie(file_name, "w");
		int row = result->GetNumRows();
		int col = result->GetNumCols();
		obuf->WriteString( StringPrintf( "%.8lf", time ) );
		obuf->WriteString( "\n" );
		for(int i=0; i<row; i++)
		{
			for(int j=0; j<col; j++)
				obuf->WriteString(StringPrintf("%.8lf", result->Get(i, j)));
			obuf->WriteString("\n");
		}

		CHECK(obuf->Flush());
		CHECK(obuf->Close());
	}

	void GPPredictor::SaveResult2(const char* file_name, NormalMatrix * mean, NormalMatrix * var, double time = 0)
	{
		File* obuf = File::OpenOrDie(file_name, "w");
		int row = mean->GetNumRows();
		obuf->WriteString( StringPrintf( "%.8lf %.8lf %.8lf", time, time, time ) );
		obuf->WriteString( "\n" );
		for(int i=0; i<row; i++)
		{
			obuf->WriteString(StringPrintf("%.8lf %.8lf %.8lf", label_[i], mean->Get(i, 0), var->Get(0,i) ));
			obuf->WriteString("\n");
		}

		CHECK(obuf->Flush());
		CHECK(obuf->Close());
	}
	void GPPredictor::PredictVariance(NormalMatrix *&result)
	{
		ParallelInterface* interface = ParallelInterface::GetParallelInterface();
		int myid = interface->GetProcId();

		if(myid == 0)
			cout << "Computing t1 = L' y." << endl;

		int numLocalRows = train_doc_.GetLocalNumberRows();
		int numGlobalRows = train_doc_.GetGlobalNumberRows();
		int numTestRows = test_doc_.GetGlobalNumberRows();
		int rank = model_.get_rank();
		double sigma2 = model_.get_sigma() * model_.get_sigma();

		ParallelMatrix * kMat = new ParallelMatrix(numGlobalRows, numTestRows); // #1
		const COV * cov = model_.get_cov();
		for(int i=0; i<numLocalRows; i++) {
			for(int j=0; j<numTestRows; j++) {
				double tmp = cov->CalcCov(*train_doc_.GetLocalSample(i), *test_doc_.GetLocalSample(j));
				kMat->Set(i, j, tmp);
			}
		}

		const ParallelMatrix * icf = model_.get_icf_res();
		NormalMatrix * product = new NormalMatrix(); // #2

		PredictingTimeProfile::ltk.Start();
		MatrixManipulation::PlPlProduct(*icf, *kMat, product);
		PredictingTimeProfile::ltk.Stop();

		NormalMatrix* tpProduct = new NormalMatrix( numTestRows, rank ); // #3
		for( int i = 0; i < numTestRows; i++ ) {
			for( int j = 0; j < rank; j++ ) {
				tpProduct->Set( i, j, product->Get( j, i ) );
			}
		}
		if(myid == 0)
			cout << "End of First Multiplication" << endl;


		PredictingTimeProfile::chols.Start();
		int buf_size = product->GetNumRows() * product->GetNumCols();
		double * buf = new double[buf_size]; // #4
		if(myid == 0) {
			NormalMatrix::PackSample(buf, product, rank, numTestRows);
			interface->Bcast(buf, buf_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
		}
		else {
			interface->Bcast(buf, buf_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
			NormalMatrix::UnpackSample(product, buf, rank, numTestRows);
		}
		delete[] buf; //#3

		int splitPart = interface->ComputeNumLocal( numTestRows );
		NormalMatrix* partProduct = new NormalMatrix( rank, splitPart); // #4
		int startInd = interface->ComputeStartInd( numTestRows );
		for( int i = 0; i < rank; i++ ) {
			for( int j = 0; j < splitPart; j++ ) {
				partProduct->Set( i, j, product->Get( i, j+startInd ) );
			}
		}
		delete product; // #3

		NormalMatrix * HX = new NormalMatrix(); // #4
		MatrixManipulation::CholForwardSub(*(model_.get_cf_res()), partProduct, HX);
		MatrixManipulation::CholBackwardSub(*(model_.get_cf_res()), HX, partProduct);   //the result of sollving HH'X = B is stored in product
		delete HX; //#3

		if(myid == 0)
			cout << "End of Matrix Inverse" << endl;

		product = new NormalMatrix( rank, numTestRows ); //#4
		for( int i = 0; i < rank; i++ ) {
			for( int j = 0; j < numTestRows; j++ ) {
				product->Set( i, j, 0 );
			}
		}
		for( int i = 0; i < rank; i++ ) {
			for( int j = 0; j < splitPart; j++ ) {
				product->Set( i, j + startInd, partProduct->Get( i, j ) );
			}
		}
		delete partProduct; //#3


		buf = new double[buf_size]; //#4
		double * bufSum = new double[buf_size]; //#5
		NormalMatrix::PackSample(buf, product, rank, numTestRows);
		interface->Reduce( buf, bufSum, buf_size, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
		NormalMatrix::UnpackSample(product, bufSum, rank, numTestRows);
		delete [] buf, delete [] bufSum; //#3
	

		PredictingTimeProfile::chols.Stop();

		PredictingTimeProfile::lm.Start();
		if(myid == 0)
			cout << "Start of RES 1" << endl;
		NormalMatrix * res1 = new NormalMatrix( 1, numTestRows ); //#4
		int localRows = kMat->GetNumLocalRows();
		for( int i = 0; i < numTestRows; i++ ) {
			double sum = 0;
			for( int j = 0; j < localRows; j++ ) {
				sum += kMat->Get( j, i ) * kMat->Get( j, i );
			}
			sum = sum / sigma2;
			res1->Set( 0, i, sum );
		}
		delete kMat; //#3

		buf_size = numTestRows;
		buf = new double[buf_size];
		bufSum = new double[buf_size]; //#5
		NormalMatrix::PackSample( buf, res1, 1, numTestRows );
		interface->Reduce( buf, bufSum, buf_size, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD );
		NormalMatrix::UnpackSample( res1, bufSum, 1, numTestRows );
		delete [] buf, delete [] bufSum; //#3

		PredictingTimeProfile::lm.Stop();


		PredictingTimeProfile::lst.Start();
		NormalMatrix * res2 = new NormalMatrix( 1, numTestRows ); //#4
		for( int i = 0; i < numTestRows; i++ ) {
			double sum = 0;
			for( int j = 0; j < rank; j++ ) {
				sum += tpProduct->Get( i, j ) * product->Get( j, i );
			}
			sum = sum / (sigma2 * sigma2);
			res2->Set( 0, i, sum );
		}
		delete tpProduct, delete product; //#2

		NormalMatrix * final_res = new NormalMatrix( 1, numTestRows );
		for(int i = 0; i < numTestRows; i++) {
			double tmp = cov->CalcCov(*test_doc_.GetLocalSample(i), *test_doc_.GetLocalSample(i) ) + sigma2;
			final_res->Set( 0, i, tmp - res1->Get(0, i) + res2->Get(0, i) );
		}
		delete res1, delete res2; //#0
		result = final_res;

		PredictingTimeProfile::lst.Stop();
		if(myid == 0)
			cout << "End of Variance" << endl;
	}


	void GPPredictor::PredictVariance2(NormalMatrix* & result) {
		ParallelInterface* interface = ParallelInterface::GetParallelInterface();
		int myid = interface->GetProcId();

		if(myid == 0)
			cout << "Start to get variance" << endl;

		int numTrRows = train_doc_.GetGlobalNumberRows();
		int numTeRows = test_doc_.GetGlobalNumberRows();
		int numLocTeRows = test_doc_.GetLocalNumberRows();
		int rank = model_.get_rank();
		int numProcs = interface->GetNumProcs();
		double sigma2 = model_.get_sigma() * model_.get_sigma();

		const ParallelMatrix * icf = model_.get_icf_res();
		NormalMatrix * product = new NormalMatrix();
		alltCovMat_->formatRow( numProcs );
		PredictingTimeProfile::ltk.Start();
		MatrixManipulation::TplNmProduct( *icf, *alltCovMat_, product );
		PredictingTimeProfile::ltk.Stop();

		int npRow = product->GetNumRows();
		int npCol = product->GetNumCols();
		int buf_size = npRow * npCol;
		double * buf = new double[buf_size];
		if(myid == 0) {
			NormalMatrix::PackSample(buf, product, npRow, npCol);
			interface->Bcast(buf, buf_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
		}
		else {
			interface->Bcast(buf, buf_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
			NormalMatrix::UnpackSample(product, buf, npRow, npCol);
		}
		delete[] buf;

		if(myid == 0)
			cout << "finish first multiplication" << endl;

		product->formatColumn( numProcs );

		NormalMatrix* tpProduct = new NormalMatrix( numLocTeRows, rank );
		NormalMatrix* opProduct = new NormalMatrix( rank, numLocTeRows );
		int startInd = interface->ComputeStartInd( numTeRows );
		for( int i = 0; i < numLocTeRows; i++ ) {
			for( int j = 0; j < rank; j++ ) {
				tpProduct->Set( i, j, product->Get( j, i+startInd ) );
				opProduct->Set( j, i, product->Get( j, i+startInd ) );
			}
		}
		delete product;
		PredictingTimeProfile::chols.Start();
		NormalMatrix * HX = new NormalMatrix();
		MatrixManipulation::CholForwardSub(*(model_.get_cf_res()), opProduct, HX);
		MatrixManipulation::CholBackwardSub(*(model_.get_cf_res()), HX, opProduct );   //the result of sollving HH'X = B is stored in product
		PredictingTimeProfile::chols.Stop();
		delete HX;

		if(myid == 0)
			cout << "Finish Computing t1 = L' y." << endl;
		PredictingTimeProfile::lm.Start();
		NormalMatrix * halfRes = new NormalMatrix(1, numLocTeRows);
		for( int i = 0; i < numLocTeRows; i++ ) {
			double sum = 0;
			for( int j = 0; j < rank; j++ ) {
				double tmp1 = tpProduct->Get( i, j );
				double tmp2 = opProduct->Get( j, i );
				sum += tmp1 * tmp2;
			}
			halfRes->Set( 0, i, sum );
		}
		delete tpProduct, delete opProduct;
		PredictingTimeProfile::lm.Stop();

		if(myid == 0)
			cout << "compute local values" << endl;

		PredictingTimeProfile::lst.Start();
		int sInd = interface->ComputeStartInd( numTeRows );
		const COV * cov = model_.get_cov();
		buf = new double [numTeRows];
		for( int i = 0; i < numTeRows; i++ ) {
			buf[i] = 0;
		}
		for( int j = 0; j < numLocTeRows; j++ ) {
			double sum = 0;
			for(int i=0; i<numTrRows; i++) {
				double tmp = covMat_->Get( j, i );
				sum += tmp * tmp;
			}

			double t1 = halfRes->Get( 0, j ) / sigma2;
			double t2 = sum;
			double t3 = ( t2 - t1 ) / sigma2; 
			if( myid == 0 && j < 10 ) {
				cout << t1 << endl;
			}
			double tmp = cov->CalcCov(*test_doc_.GetLocalSample(j), *test_doc_.GetLocalSample(j)) + sigma2;
			buf[ sInd + j ] = tmp - t3;
		}
		if(myid == 0)
			cout << "End" << endl;
		double* res = new double [numTeRows];
		interface->Reduce( buf, res, numTeRows, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD );
		NormalMatrix* kstar= new NormalMatrix( 1, numTeRows );
		NormalMatrix::UnpackSample( kstar, res, 1, numTeRows);
		kstar->formatColumnBack( numProcs );
		PredictingTimeProfile::lst.Stop();
		delete[] buf, delete res;
		result = kstar;
		if(myid == 0)
			cout << "End2" << endl;
	}
	void GPPredictor::PredictMean2(NormalMatrix *&result)
	{
		ParallelInterface* interface = ParallelInterface::GetParallelInterface();
		int myid = interface->GetProcId();
		int numProcs = interface->GetNumProcs();

		if(myid == 0)
			cout << "Computing t1 = L' y." << endl;
		double mean = model_.get_mean();

		int numTrRows = train_doc_.GetGlobalNumberRows();

		int numCols = 1;
		NormalMatrix* yMat = new NormalMatrix( numTrRows, numCols);
		double* label = new double[numTrRows];
		train_doc_.GetLocalLabels(label);
		for(int i=0; i< numTrRows; i++){
			yMat->Set(i, 0, label[i] - mean); // BECAREful, I changed!
		}
		delete label;
		yMat->formatRow( numProcs );
		const ParallelMatrix * icf = model_.get_icf_res();
		NormalMatrix * product = new NormalMatrix();
		MatrixManipulation::TplNmProduct(*icf, *yMat, product);

		if(myid == 0)
		{
			NormalMatrix * HX = new NormalMatrix();
			MatrixManipulation::CholForwardSub(*(model_.get_cf_res()), product, HX);
			MatrixManipulation::CholBackwardSub(*(model_.get_cf_res()), HX, product);   //the result of sollving HH'X = B is stored in product
			delete HX;
		}

		int rank = model_.get_rank();
		int buf_size = rank* numCols;
		double * buf = new double[buf_size];
		if(myid == 0) {
			NormalMatrix::PackSample(buf, product, rank, numCols);
			interface->Bcast(buf, buf_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
		}
		else {
			interface->Bcast(buf, buf_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
			NormalMatrix::UnpackSample(product, buf, rank, numCols);
		}
		delete[] buf;

		NormalMatrix* res = new NormalMatrix();
		MatrixManipulation::PlNmProduct(*icf, *product, res);

		if( myid == 0 ) {
			for(int i=0; i<numTrRows; i++) {
				for(int j=0; j<numCols; j++) {
					double tmp1 = yMat->Get(i, j);
					double tmp2 = res->Get(i, j)/(model_.get_sigma() * model_.get_sigma());
					double tmp3 = (tmp1 - tmp2)/(model_.get_sigma() * model_.get_sigma());
					res->Set(i, j, tmp3);
				}
			}
		}

		buf_size = numTrRows * numCols;
		buf = new double[buf_size];
		if(myid == 0) {
			NormalMatrix::PackSample(buf, res, buf_size, numCols);
			interface->Bcast(buf, buf_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
		}
		else {
			interface->Bcast(buf, buf_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
			NormalMatrix::UnpackSample( res, buf, buf_size, numCols);
		}
		delete[] buf, delete product;

		int numTestRows = test_doc_.GetGlobalNumberRows();
		int numTeLocalRows = test_doc_.GetLocalNumberRows();
		ParallelMatrix * kMat = new ParallelMatrix( numTestRows, numTrRows);
		const COV * cov = model_.get_cov();
		for(int i=0; i<numTeLocalRows; i++) {
			for(int j=0; j<numTrRows; j++) {
				double tmp = cov->CalcCov(*test_doc_.GetLocalSample(i), *train_doc_.GetLocalSample(j));
				kMat->Set(i, j, tmp ); //BECAREful, I changed !
			}
		}
		NormalMatrix * final_res = new NormalMatrix();
		res->formatRowBack( numProcs );
		MatrixManipulation::PlNmProduct(*kMat, *res, final_res);
		int nr = final_res->GetNumRows();
		int nc = final_res->GetNumCols();
		if( myid == 0 ) {
			for( int i = 0; i < nr; i++ ) {
				for( int j = 0; j < nc; j++ ) {
					double v = final_res->Get( i, j );
					final_res->Set(i, j, v + mean );
				}
			}
		}
		final_res->formatRowBack( numProcs );
		delete kMat;
		delete res;
		result = final_res;
	}

	void GPPredictor::PredictMean(NormalMatrix *&result)
	{
		ParallelInterface* interface = ParallelInterface::GetParallelInterface();
		int myid = interface->GetProcId();

		if(myid == 0)
			cout << "Computing t1 = L' y." << endl;
		double mean = model_.get_mean();

		int numLocalRows = train_doc_.GetLocalNumberRows();
		int numGlobalRows = train_doc_.GetGlobalNumberRows();
		int numCols = 1;
		ParallelMatrix * yMat = new ParallelMatrix(numGlobalRows, numCols);

		double * label = new double[numLocalRows];
		train_doc_.GetLocalLabels(label);
		for(int i=0; i<numLocalRows; i++)
			yMat->Set(i, 0, label[i] - mean); // BECAREful, I changed!
		delete[] label;

		const ParallelMatrix * icf = model_.get_icf_res();
		NormalMatrix * product = new NormalMatrix();
		MatrixManipulation::PlPlProduct(*icf, *yMat, product);

		if(myid == 0)
		{
			NormalMatrix * HX = new NormalMatrix();
			MatrixManipulation::CholForwardSub(*(model_.get_cf_res()), product, HX);
			MatrixManipulation::CholBackwardSub(*(model_.get_cf_res()), HX, product);   //the result of sollving HH'X = B is stored in product
			delete HX;
		}

		int rank = model_.get_rank();
		int buf_size = rank * numCols;
		double * buf = new double[buf_size];
		if(myid == 0) {
			NormalMatrix::PackSample(buf, product, rank, numCols);
			interface->Bcast(buf, buf_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
		}
		else {
			interface->Bcast(buf, buf_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
			NormalMatrix::UnpackSample(product, buf, rank, numCols);
		}
		delete[] buf;


		ParallelMatrix * res = new ParallelMatrix();
		MatrixManipulation::PlNmProduct(*icf, *product, res);
		delete product;
		for(int i=0; i<numLocalRows; i++) {
			for(int j=0; j<numCols; j++) {
				double tmp1 = yMat->Get(i, j);
				double tmp2 = res->Get(i, j)/(model_.get_sigma() * model_.get_sigma());
				double tmp3 = (tmp1 - tmp2)/(model_.get_sigma() * model_.get_sigma());
				res->Set(i, j, tmp3);
			}
		}

		int numTestRows = test_doc_.GetGlobalNumberRows();
		ParallelMatrix * kMat = new ParallelMatrix(numGlobalRows, numTestRows);
		const COV * cov = model_.get_cov();
		for(int i=0; i<numLocalRows; i++) {
			for(int j=0; j<numTestRows; j++) {
				double tmp = cov->CalcCov(*train_doc_.GetLocalSample(i), *test_doc_.GetLocalSample(j));
				kMat->Set(i, j, tmp ); //BECAREful, I changed !
			}
		}
		NormalMatrix * final_res = new NormalMatrix();
		MatrixManipulation::PlPlProduct(*kMat, *res, final_res);
		int nr = final_res->GetNumRows();
		int nc = final_res->GetNumCols();
		for( int i = 0; i < nr; i++ ) {
			for( int j = 0; j < nc; j++ ) {
				double v = final_res->Get( i, j );
				final_res->Set(i, j, v + mean );
			}
		}
		delete kMat;
		delete res;
		result = final_res;
	}
}
using namespace psvm;

//=============================================================================
// Parameter Definitions

string FLAGS_model_path = ".";
string FLAGS_train_file = "./train";
string FLAGS_test_file = "./test";
string FLAGS_mean_file = "./result.mean";
string FLAGS_variance_file = "./result.variance";
//=============================================================================

void Usage() {
	const char* msg =
		"gp_predict: This program predicts the class labels of samples. Usage:\n"
		"  gp_predict data_file\n"
		"The predict result is saved in data_file.predict.\n"
		"\n"
		"  Flag descriptions:\n"
		"    -rd(training_data)      type: string\n"
		"    -ed(test_data)          type: string\n"
		"    -mf(mean_file)          type: string\n"
		"    -vf(variance_file)      type: string\n"
		"    -mp(model_path) (Directory where to load the stored model.) type: string\n";
	cerr << msg;
}

void ParseCommandLine(int argc, char** argv) 
{
	if( argc < 3 ) {
		Usage();
		exit( 2 );
	}
	for(int i=1; i<argc; i+=2)
	{
		if(i + 1 >= argc)
			break;
		if(argv[i][0] != '-')
			break;
		char* param_name = argv[i] + 1;
		char* param_value = argv[i+1];

		if (strcmp(param_name, "mp") == 0) 
			FLAGS_model_path = string(param_value);
		else if (strcmp(param_name, "rd") == 0) 
			FLAGS_train_file = string(param_value);
		else if (strcmp(param_name, "ed") == 0) 
			FLAGS_test_file = string(param_value);
		else if (strcmp(param_name, "mf") == 0) 
			FLAGS_mean_file = string(param_value);
		else if (strcmp(param_name, "vf") == 0)
			FLAGS_variance_file = string(param_value);
		else {
			cerr << "Unknown parameter " << param_name << endl;
			Usage();
			exit(2);
		}
	}
}

int main(int argc, char** argv) 
{
	// Initializes the parallel computing environment
	ParallelInterface* interface = ParallelInterface::GetParallelInterface();
	interface->Init(&argc, &argv);

	ParseCommandLine(argc, argv);

	// Begin Timing
	PredictingTimeProfile::total.Start();

	GPPredictor predictor;


	// Predict
	PredictingTimeProfile::predict.Start();
	predictor.Predict( FLAGS_model_path.c_str(), FLAGS_train_file.c_str(), FLAGS_test_file.c_str(),
			FLAGS_mean_file.c_str(), FLAGS_variance_file.c_str());
	PredictingTimeProfile::predict.Stop();

	// End Timing
	PredictingTimeProfile::total.Stop();

	// Save time info
	interface->Barrier(MPI_COMM_WORLD);
	if (interface->GetProcId() == 0) {
		cout << "Saving predicting time statistic info ... " << endl;
	}
	//predictor.SaveTimeInfo(FLAGS_model_path.c_str(), "PredictingTimeInfo");

	// Prints the statistical information.
	interface->Barrier(MPI_COMM_WORLD);
	if (interface->GetProcId() == 0) {
		// Print timing info
		cout << 
			endl << predictor.PrintTimeInfo() << endl;
		//<< "========== Predict Matrix ==========" << endl
		//<< "========== Predict Accuracy ==========" << endl;
	}

	// Finalizes the parallel computing environment
	interface->Finalize();
	return 0;
}
std::string GPPredictor::PrintTimeInfo() {
	std::string str = "========== Predicting Time Statistics ==========\n";
	str += "Total                       : " + PredictingTimeProfile::total.PrintInfo() + "\n";
	str += "1. Load Model               : " + PredictingTimeProfile::read_model.PrintInfo() + "\n";
	str += "2. Load Data                : " + PredictingTimeProfile::read_doc.PrintInfo() + "\n";
	str += "3. Predict                  : " + PredictingTimeProfile::predict.PrintInfo() + "\n";
	str += "   3.1 Predict Mean         : " + PredictingTimeProfile::premean.PrintInfo() + "\n";
	str += "   3.2 Predict Var          : " + PredictingTimeProfile::prevar.PrintInfo() + "\n";
	str += "     3.2.1  ltk             : " + PredictingTimeProfile::ltk.PrintInfo() + "\n" ;
	str += "     3.2.2  chols           : " + PredictingTimeProfile::chols.PrintInfo() + "\n";
	str += "     3.3.3  lm              : " + PredictingTimeProfile::lm.PrintInfo() + "\n";
	str += "     3.3.4  last            : " + PredictingTimeProfile::lst.PrintInfo() + "\n";
	str += "     3.3.5  calc            : " + PredictingTimeProfile::calc.PrintInfo() + "\n";
	return str;
}
void GPPredictor::SaveTimeInfo(const char *path, const char* file_name) 
{
	ParallelInterface *mpi = ParallelInterface::GetParallelInterface();
	int proc_id   = mpi->GetProcId();

	// Open file file_name.# for writing
	char filename[4096];
	snprintf(filename, sizeof(filename), "%s/%s.%d", path, file_name, proc_id);
	File* obuf = File::OpenOrDie(filename, "w");

	std::string str = PrintTimeInfo();
	CHECK(obuf->WriteString(str) == str.length());

	CHECK(obuf->Flush());
	CHECK(obuf->Close());
	delete obuf;
}


