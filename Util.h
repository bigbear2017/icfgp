#pragma once

#define _USE_MATH_DEFINES 

#include <fstream>
#include <cmath>
#include <complex>
#include <iostream>
#include <iomanip>
#include <vector>
#include <limits>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <fcntl.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <math.h> 
#include <algorithm>

using namespace std;

void      init_random   ( void );
void      init_random   ( unsigned int ID );
int       random        ( int K);
int*      random_perm   ( int N, int K);


double*   new_VecDoub   ( int len );
int*      new_VecInt    ( int len );
void      del_VecDoub   ( double* v, int len );
void      del_VecInt    ( int* v, int len );
void      print_VecDoub ( double* v, int len );
void      print_VecInt  ( int* v, int len );

double    dot           ( double* a, double* b, int len );
double    norm          ( double* v, int len );
double    distance      ( double* u, double* v, int len);
double    average       ( double* v, int len );
double    normalize     ( double* p, int len );
double    get_entropy   ( double* p, int len );
void      copy          ( double* from, double* to, int len );

int       max_index     ( double* a, int len );

double**  new_MatDoub   ( int row, int col );
int**     new_MatInt    ( int row, int col );
void      del_MatDoub   ( double** m, int row, int col );
void      del_MatInt    ( int** m, int row, int col );
void      print_MatDoub ( double** m, int row, int col );
void      print_MatInt  ( int** m, int row, int col );

void      cholesky      ( double** m, double** L, int row, int col );
void      inverse       ( double** L, double** inv, int row, int col); 
void      solve         ( double** L, double* b, double* x, int len);
void      elsolve       ( double** L, double* b, double* y, int len);
void      elsolve       ( double** L, double** B, double** Y, int len,int col_b);
double    logdet        ( double** L, int row, int col);

vector<string> splitEx  ( string& src, string separate_character );
