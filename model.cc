#include <cstring>
#include <iostream>
#include <vector>
#include <utility>
#include <string>
#include "model.h"
#include "document.h"
#include "cov.h"
#include "matrix.h"
#include "io.h"
#include "util.h"
#include "parallel_interface.h"
namespace psvm {
Model::Model() {
	mpi_ = ParallelInterface::GetParallelInterface();
	cf_res_ = new LLMatrix();
	icf_res_ = new ParallelMatrix();
}

Model::~Model() {
	delete cf_res_;
	delete icf_res_;
}

// Saves model in the "str_directory/" directory.
// several files will be created.
//   model.header: stores general information, including:kernel parameters.
//   model: matrix files
void Model::Save(const char* str_directory, const char* model_name) {
	if (mpi_->GetProcId() ==0) {
		cout << "========== Store Model ==========" << endl;
		SaveHeader(str_directory, model_name);
		cout << "Header saving end!----------------------------" << endl;
	}
	mpi_->Barrier(MPI_COMM_WORLD);

	cout << "Matrix saving start---------------------------" << endl;

	SaveChunks(str_directory, model_name);

	cout << "Matrix saving end!-----------------------------" << endl;
}

void Model::SaveHeader(const char* str_directory, const char* model_name) {
	if(mpi_->GetProcId() != 0)
		return;
	char str_file_name[4096];

	// Create a file for storing model.header
	snprintf(str_file_name, sizeof(str_file_name), "%s/%s.header", str_directory, model_name);
	File* obuf = File::OpenOrDie(str_file_name, "w");

	// Output kernel parameters
	cout << "Storing " << model_name << ".header ... " << endl;
	obuf->WriteString(StringPrintf("%.8lf %d %.8lf %.8lf ", trtime_, rank_, sigma_, mean_ ));

	const std::string str = cov_->get_parm_str();
	obuf->WriteString(str);

	CHECK(obuf->Flush());
	CHECK(obuf->Close());
	delete obuf;
}

void Model::SaveChunks(const char* str_directory, const char* model_name) {
	cout << "Storing " << model_name << endl;
	std::string cf_name = string(model_name) + ".cf";
	std::string icf_name = string(model_name) + ".icf";
	if( mpi_ -> GetProcId() == 0 ) {
		cf_res_->Save(str_directory, cf_name.c_str());
	}
	icf_res_->Save(str_directory, icf_name.c_str());
}

// Loads model from the "str_directory/" directory. Two files will be read
//   model.header: stores general information, including:
//        kernel parameters.
//   model: matrix content
void Model::Load(const char* str_directory, const char* model_name) {
	cout << "=================== Load Model ===================" << endl;
	LoadHeader(str_directory, model_name);
	cout << "Finish Loading header!" << endl;
	LoadChunks(str_directory, model_name);
	cout << "Finish loading chuncks!" << endl;
}

void Model::LoadHeader(const char*str_directory, const char* model_name) 
{
	char str_file_name[4096];
	// Load model header file
	snprintf(str_file_name, sizeof(str_file_name), "%s/%s.header", str_directory, model_name);
	string line;
	File *reader = File::OpenOrDie(str_file_name, "r");

	int rank;
	double sigma;
	double mean;
	double trtime;

	reader->ReadLine(&line);
	const char *buf = line.c_str();


	SplitOneDoubleToken(&buf, " ", &trtime);
	SplitOneIntToken(&buf, " ", &rank);
	SplitOneDoubleToken(&buf, " ", &sigma);
	SplitOneDoubleToken(&buf, " ", &mean );
	trtime_ = trtime;
	rank_ = rank;
	sigma_ = sigma;
	mean_ = mean;

	int dim; 
	double sf;
	SplitOneDoubleToken(&buf, " ", &sf);
	SplitOneIntToken(&buf, " ", &dim);
	double * ell = new double[ dim ];
	for( int i = 0; i < dim; i++ ) {
		SplitOneDoubleToken(&buf, " ", &ell[i]);
	}
	cov_ = new CovSEISO(sf, dim, ell);
	reader->Close();
	delete reader;
	delete [] ell;
}
void Model::LoadChunks(const char*str_directory, const char* model_name) 
{
	string cf_name = string(model_name) + ".cf";
	string icf_name = string(model_name) + ".icf";
	cf_res_->Load(str_directory, cf_name.c_str());
	icf_res_->Load(str_directory, icf_name.c_str());
}
}
