#ifndef TRAIN_HYPER
#define TRAIN_HYPER
#include <string>
#include "gp_parm.h"

namespace psvm {
	class Document;
	struct HyParam;
	class HyperTrainer {
		public:
			HyperTrainer( HyParam& hp );
			void TrainModel( int maxIter );
			std::string PrintTimeInfo();
			void SaveTimeInfo();
			void ReadData();
			void evaluateNlmlGrad( double * h , double & f,double* df);
			Document _normalDoc;
			Document _allDoc;
		private:
			HyParam _param;
			int _numProcs; //
			double * _hyp;
			ParallelInterface * _interface;
	};
}
#endif
