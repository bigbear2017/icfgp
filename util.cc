/*
Copyright 2007 Google Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "util.h"
#include <cstring>
#include <stdio.h>
#include <ctype.h>

namespace psvm {
void Log(const string& msg, const string& file, int linenum) {
  char buff[100];
  sprintf(buff, "%d", linenum);
  cerr << msg << " in file " << file << " at line " << buff << endl;
}

bool SplitOneIntToken(const char** source, const char* delim,
                      int* value) {

  int i = 0;
  const char* buf = *source;

  // Make sure the first character is a digit and parse it
  if (!isdigit(buf[0])) {
    if (buf[0] == '-' || buf[0] == '+') {
      if (!isdigit(buf[1])) return false;
      i = 1;
    } else {
      return false;
    }
  }
  if (sscanf(buf, "%d", value) != 1) return false;

  // Skip the integer
  for (; buf[i] != 0 && isdigit(buf[i]); ++i);

  // Make sure the character after the integer is from delim and skip it
  if (buf[i] != 0) {
    if (strchr(delim, buf[i]) == NULL) return false;
  }
  *source = &(buf[i + 1]);

  return true;
}

bool SplitOneDoubleToken(const char** source, const char* delim,
                         double* value) {
  int i = 0;
  const char* buf = *source;

  // Make sure the first character is a digit and parse it
  if (!isdigit(buf[0])) {
    if (buf[0] == '-' || buf[0] == '+') {
      if (!isdigit(buf[1])) return false;
      i = 1;
    } else {
      return false;
    }
  }
  if (sscanf(buf, "%lf", value) != 1) return false;

  // Skip the number
  while (buf[i] != 0 && strchr(delim, buf[i]) == NULL) ++i;

  // Make sure the character after the number is from delim and skip it
  if (buf[i] != 0) {
    if (strchr(delim, buf[i]) == NULL) return false;
  }
  *source = &(buf[i + 1]);

  return true;
}

// Split a line into a vector of <key, value> pairs. The line has
// the following format:
//
// <kvpsep>*<key1><kvsep>+<value1><kvpsep>+<key2><kvsep>+<value2>...<kvpsep>*
void SplitStringIntoKeyValuePairs(const string& line,
                                  const string& key_value_delimiters,
                                  const string& key_value_pair_delimiters,
                                  vector<pair<string, string> >* kv_pairs) {
  int i, j;
  const char* buf = line.c_str();
  kv_pairs->clear();

  // Skip the preceding kvpsep
  for (i = 0; buf[i] != 0; ++i) {
    if (strchr(key_value_pair_delimiters.c_str(), buf[i]) == NULL) break;
  }

  while (true) {
    // Locate the key
    j = i;
    while (buf[j] != 0 && strchr(key_value_delimiters.c_str(), buf[j]) == NULL) ++j;
    if (buf[j] == 0) break;
    string key(&buf[i], j - i);

    // Skip the kvsep
    i = j;
    while (buf[j] != 0 && strchr(key_value_delimiters.c_str(), buf[j]) != NULL) ++j;
    if (buf[j] == 0 || i == j) break;

    // Locate the value
    i = j;
    while (buf[j] != 0 && strchr(key_value_pair_delimiters.c_str(), buf[j]) == NULL) ++j;
    if (i == j) break;
    string value(&buf[i], j - i);

    kv_pairs->push_back(make_pair(key, value));

    // Skip the kvpsep
    for (i = j; buf[i] != 0; ++i) {
      if (strchr(key_value_pair_delimiters.c_str(), buf[i]) == NULL) break;
    }
  }
}

string StringPrintfV(const char* format, va_list ap) {
  char buffer[1024];
  va_list backup_ap;
  va_copy(backup_ap, ap);
  vsnprintf(buffer, sizeof(buffer), format, backup_ap);
  va_end(backup_ap);
  return string(buffer);
}

string StringPrintf(const char* format, ...) {
  va_list ap;
  va_start(ap, format);
  string result = StringPrintfV(format, ap);
  va_end(ap);
  return result;
}

/*--------------------Matrix Util------------------*/


/*---------------------random---------------------*/

void init_random( void )
{
	time_t t;
	srand( time( &t ) ); 
}

void init_random( unsigned int ID )
{
	srand( ID );
}

int random( int K )
{
	// return a random value in [0,K)
	return rand() % K;
}

int* random_perm( int N, int K )
{
	int i,r;
	int* visit = new_VecInt( N );
	int* perm = new_VecInt( K );
	
	for ( i=0; i<N; i++ )
		visit[i]=0;
	
	for ( i=0; i<K; ) {
		r = rand() % N;
		if( visit[r] == 1 ) continue;
		visit[r]=1;
		perm[i++] = r;
	}

	del_VecInt( visit, N );
	return perm;
}

/*---------------------vector---------------------*/

double* new_VecDoub( int len )
{
	double* m = new double[len];
	return m;
}

void del_VecDoub( double* v, int len )
{
	delete[] v;
}

int* new_VecInt( int len )
{
	int* m = new int[len];
	return m;
}

void del_VecInt( int* v, int len )
{
	delete[] v;
}

void print_VecDoub( double* v, int len )
{
	for( int i=0; i<len; i++ ) printf("%.4lf ",v[i]);
	printf("\r\n\r\n");
}

void print_VecInt( int* v, int len )
{
	for( int i=0; i<len; i++ ) printf("%d ",v[i]);
	printf("\r\n\r\n");
}

double dot( double* a, double* b, int len )
{
	double sum = 0.0;
	for(int i=0; i<len; i++ ) sum += a[i] * b[i];
	return sum;
}

double norm( double* v, int len)
{
	double sum = 0.0;
	for( int i=0; i<len; i++) sum += v[i] * v[i];
	return sqrt( sum );
}

double distance( double* u, double* v, int len)
{
	double sum = 0.0;
	for( int i=0; i<len; i++) sum += ( u[i] - v[i] ) * ( u[i] - v[i] );
	return sqrt( sum );
}

double average( double* v, int len)
{
	double sum = 0.0;
	for( int i=0; i<len; i++) sum += v[i];
	return sum/len;
}

double normalize( double* p, int len )
{
	double sum = 0.0;
	for( int i=0; i<len; i++) sum += p[i];
	for( int i=0; i<len; i++) p[i] /= sum;
	return sum;
}

double get_entropy( double* p, int len )
{
	double ent = 0.0;
	for( int i=0; i<len; i++ ) {
		if( p[i] == 0.0 ) ent += 0.0;
		else ent += -p[i] * log(p[i]);
	}
	return ent;
}

void copy ( double* from, double* to, int len )
{
	for ( int i=0; i<len; i++ ) to[i] = from[i];
}

int max_index( double* a, int len )
{
	double max=-9999999;
	int index=-1;
	for( int i=0; i<len; i++ ) {
		if( a[i] > max ) {
			max = a[i];
			index=i;
		}
	}
	return index;
}

/*---------------------matrix---------------------*/

double** new_MatDoub( int row, int col )
{
	double** m = new double*[row];
	for( int i=0; i<row; i++ ) m[i] = new double[col];
	return m;
}

void del_MatDoub( double** m, int row, int col )
{
	for( int i=0; i<row; i++) delete[] m[i];
	delete[] m;
}

int** new_MatInt( int row, int col )
{
	int** m = new int*[row];
	for( int i=0; i<row; i++) m[i] = new int[col];
	return m;
}

void del_MatInt( int** m, int row, int col )
{
	for( int i=0; i<row; i++ ) delete[] m[i];
	delete[] m;
}

void print_MatDoub( double** m, int row, int col )
{
	for( int i=0; i<row; i++ ){
		for( int j=0; j<col; j++ ) printf("%.4lf ",m[i][j]);
		printf("\r\n");
	}
	printf("\r\n");
}

void print_MatInt( int** m, int row, int col )
{
	for( int i=0; i<row; i++ ){
		for( int j=0; j<col; j++ ) printf("%d ",m[i][j]);
		printf("\r\n");
	}
	printf("\r\n");
}

/*---------------------cholesky---------------------*/

void cholesky(double** m,double** L,int row,int col)
{
	int i,j,k;
	int n = row;

    for ( i = 0; i < n; i++ )  {
        for ( j = 0; j <= i; j++ ) {
            double sum = 0.0;
            for (k = 0; k < j; k++ ) {
                sum += L[i][k] * L[j][k];
            }
            if (i == j) L[i][i] = sqrt(m[i][i] - sum);
            else        L[i][j] = 1.0 / L[j][j] * (m[i][j] - sum);
        }
        if (L[i][i] <= 0) printf("Matrix not positive definite");
    }

	for ( i=0; i<n; i++ )
		for ( j=0; j<i; j++ ) 
			L[j][i] = 0.;
}

void inverse( double** L, double** inv, int row, int col ) 
{
	int i,j,k;
	int n = row;

	double sum;
	for ( i=0; i<n; i++ ) 
		for (j=0; j<=i; j++ ){
			sum = (i==j? 1. : 0.);
			for ( k=i-1; k>=j; k-- ) sum -= L[i][k] * inv[j][k];
			inv[j][i] = sum / L[i][i];
		}
	for ( i=n-1; i>=0; i-- ) 
		for ( j=0; j<=i; j++) {
			sum = (i<j? 0. : inv[j][i]);
			for ( k=i+1; k<n; k++ ) sum -= L[k][i] * inv[j][k];
			inv[i][j] = inv[j][i] = sum / L[i][i];
		}
}

double logdet( double** L, int row, int col ) 
{
	double sum = 0.;
	for ( int i=0; i<row; i++ ) sum += log(L[i][i]);
	return 2.*sum;
}

void solve( double** L, double* b, double* x, int len ) 
{
	// solve LL'x=b x=L'\L\b
	int i,k;
	double sum;

	for ( i=0; i<len; i++ ) {
		sum=b[i];
		for ( k=i-1; k>=0; k-- ) sum -= L[i][k] * x[k];
		x[i]=sum/L[i][i];
	}
	for ( i=len-1; i>=0; i-- ) {
		sum=x[i];
		for ( k=i+1; k<len; k++) sum -= L[k][i] * x[k];
		x[i] = sum / L[i][i];
	}
}

void elsolve( double** L, double* b, double* y, int len ) 
{
	// solve Ly=b y=L\b
	int i,j;
	double sum;

	for ( i=0; i<len; i++ ) {
		sum=b[i];
		for ( j=0; j<i; j++ ) sum -= L[i][j] * y[j];
		y[i] = sum / L[i][i];
	}
}

void elsolve( double** L, double** B, double** Y, int len, int col_b ) 
{
	// solve Ly=b y=L\b
	int i,j,b;
	double sum;

	for ( i=0; i<len; i++ ) {
		for( b=0; b<col_b; b++ ){
			sum=B[i][b];
			for ( j=0; j<i; j++ ) sum -= L[i][j] * Y[j][b];
			Y[i][b] = sum / L[i][i];
		}
	}
}

/*---------------------string---------------------*/

vector<string> splitEx (string& src, string separate_character)   
{   
    vector<string> strs;   
       
    int separate_characterLen = separate_character.size(); 
    int lastPosition = 0;
	int index = -1;   
    while ( -1 != (index = src.find(separate_character,lastPosition) ) ) {   
        strs.push_back( src.substr(lastPosition,index - lastPosition) );   
        lastPosition = index + separate_characterLen;   
    }   
    string lastString = src.substr( lastPosition );   
    if ( !lastString.empty() )   
        strs.push_back( lastString );
    return strs;   
}   
}
