#ifndef GP_TRAIN_H__
#define GP_TRAIN_H__
#include <string>
#include "cov.h"
namespace psvm 
{
class Document;
class Model;
class GPTrainer
{
public:
	void TrainModel(const Document& doc, const GPParameter& p, const COV& cov , Model* model);
	std::string PrintTimeInfo();
	void SaveTimeInfo(const char *path, const char* file_name);
};
}
#endif
