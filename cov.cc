#include <cstdlib>
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <fstream>
#include <cstring>
#include "cov.h"
#include "document.h"
#include "matrix.h"
#include "util.h"
namespace psvm {
double COV::OneNormSub(const Sample& a, const Sample& b) const {
  double norm = 0.0;
  vector<Feature>::const_iterator it1 = a.features.begin();
  vector<Feature>::const_iterator it2 = b.features.begin();
  while (it1 != a.features.end() && it2 != b.features.end()) {
    if (it1->id == it2->id) {
      norm += fabs(it1->weight - it2->weight);
      ++it1;
      ++it2;
    } else if (it1->id < it2->id) {
      norm += fabs(it1->weight);
      ++it1;
    } else {
      norm += fabs(it2->weight);
      ++it2;
    }
  }
  while (it1 != a.features.end()) {
      norm += fabs(it1->weight);
      ++it1;
  }
  while (it2 != b.features.end()) {
      norm += fabs(it2->weight);
      ++it2;
  }
  return norm;
}

double COV::TwoNormSub(const Sample& a, const Sample& b) const {
	if(a.id == b.id)
		return 0.0;
	double norm = 0.0;
	vector<Feature>::const_iterator it1 = a.features.begin();
    vector<Feature>::const_iterator it2 = b.features.begin();
	while (it1 != a.features.end() && it2 != b.features.end()) {
    if (it1->id == it2->id) {
		double tmp = it1->weight - it2->weight;
		norm += tmp * tmp;
        ++it1;
        ++it2;
    } else if (it1->id < it2->id) {
        norm += it1->weight * it1->weight;
        ++it1;
    } else {
        norm += it2->weight * it2->weight;
        ++it2;
    }
  }
  while (it1 != a.features.end()) {
      norm += it1->weight * it1->weight;
      ++it1;
  }
  while (it2 != b.features.end()) {
      norm += it2->weight * it2->weight;
      ++it2;
  }
  return norm;
}

// The implementation of this method is almost identical to that of OneNormSub. See comment there.
double COV::InnerProduct(const Sample& a, const Sample& b) const {
  double norm = 0.0;
  vector<Feature>::const_iterator it1 = a.features.begin();
  vector<Feature>::const_iterator it2 = b.features.begin();
  while (it1 != a.features.end() && it2 != b.features.end()) {
    if (it1->id == it2->id) {
      norm += it1->weight * it2->weight;
      ++it1;
      ++it2;
    } else if (it1->id < it2->id) {
      ++it1;
    } else {
      ++it2;
    }
  }
  return norm;
}

CovSEISO::CovSEISO( const string& file ) {
	std::fstream f( file.c_str() );
	f >> sf ;
	f >> sigma;
	f >> mean;
	f >> dim;
	ell = new double[ dim ];
	for( int i = 0; i < dim; i++ ) {
		f >> ell[i];
	}
	f.close();
}

// Squared Exponential covariance function with isotropic distance measure. The coriance function is k(x^p, k^q) = sf^2 * exp(-(x^p - x^q)' * (x^p - x^q)/(2 * ell^2));
double CovSEISO::CalcCov(const Sample& a, const Sample& b) const {
	vector<Feature> fas = a.features;
	vector<Feature> fbs = b.features;

	double res = 0;
	for( int i = 0; i < dim; i++ ) {
		if( fas[i].id != fbs[i].id ) {
			cout << "cov seiso" << endl;
			cout << "error, different id" << endl;
			exit( 0 );
		}
		double rr = (fas[i].weight - fbs[i].weight) / ell[i];
		res += rr * rr;
	}
	res = -0.5 * res; 
	res = exp(res) * sf * sf;
	return res;
}
std::string CovSEISO::get_parm_str() const {
	char str_res[4096];
	sprintf( str_res, "%f %d ", sf, dim );
	for( int i = 0; i < dim; i++ ) {
		int len = strlen( str_res );
		sprintf( str_res + len, "%f ", ell[i] );
	}
	return str_res;
}

double CovSENoise::CalcCov( const Sample & a, const Sample & b ) const {
	vector<Feature> fas = a.features;
	vector<Feature> fbs = b.features;

	double res = 0;
	for( int i = 0; i < _dim; i++ ) {
		if( fas[i].id != fbs[i].id ) {
			cout << "i: " << i << endl;
			cout << "aid: " << a.id << " bid: " << b.id << endl;
			cout << "id1: " << fas[i].id << " id2: " << fbs[i].id << endl;
			cout << "cov senoise" << endl;
			cout << "error, different id" << endl;
			exit( 0 );
		}
		double rr = (fas[i].weight - fbs[i].weight) / _ell[i];
		res += rr * rr;
	}
	res = -0.5 * res; 
	res = exp(res) * _sf * _sf;
	if( a.id == b.id ) {
		res += _nf * _nf;
	}
	return res;
}

CovSEDiv::CovSEDiv( double sf, double noise, int dim, double *ell, int index ): CovSEISO( sf, dim, ell ) {
	_index = index;
	_noise = noise;
}

double CovSEDiv::CalcCov( const Sample & a, const Sample & b ) const {
	vector<Feature> fas = a.features;
	vector<Feature> fbs = b.features;

	double res = 0;
	for( int i = 0; i < this->dim; i++ ) {
		if( fas[i].id != fbs[i].id ) {
			cout << "error, different id" << endl;
			exit( 0 );
		}
		double rr = (fas[i].weight - fbs[i].weight) / ell[i];
		res += rr * rr;
	}

	if ( _index < dim ) {
		//return the value for length scales
		double d = (fas[_index].weight - fbs[_index].weight ) / ell[_index];
		d = d * d;
		return d * sf * sf * exp( -0.5 * res );
	} else if ( _index == dim ) {
		//return the value for signal variance
		return 2 * sf * sf * exp( -0.5 * res );
	} else {
		//return the value for noise variance
		if( a.id == b.id ) {
			return 2 * _noise * _noise;
		} else 
			return 0;
	}
}
/*
void CovSEISO::set_parm(std::string str) {
	cout << "Begin " << endl;
	const char * buf = str.c_str();
	cout << buf << endl;
	cout << "EEEEEEEEEEEEEEE" << endl;
	int type;
	SplitOneIntToken(&buf, " ", &type);
	covType_ = (COVType)type;
	SplitOneDoubleToken(&buf, " ", &sf_);
	SplitOneDoubleToken(&buf, " ", &ell_);
}
*/
}
