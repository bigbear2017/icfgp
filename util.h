/*
Copyright 2007 Google Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef UTIL_H__
#define UTIL_H__

#include <fstream>
#include <cmath>
#include <complex>
#include <iostream>
#include <iomanip>
#include <vector>
#include <limits>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <fcntl.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <math.h> 
#include <algorithm>
#include <stdarg.h>

using namespace std;

#define CHECK(condition)  \
                  if (!(condition)) { Log("Check failed: " #condition " ",  __FILE__, __LINE__) ; exit(1);}

#define CHECK_LE(a, b)  \
                  if ((a) > (b)) { Log("Check failed: " #a " <= " #b " ",  __FILE__, __LINE__) ; exit(1);}

#define CHECK_EQ(a, b)  \
                  if ((a) == (b)) { Log("Check failed: " #a " == " #b " ",  __FILE__, __LINE__) ; exit(1);}


namespace psvm {
void Log(const string& msg, const string& file, int linenum);

bool SplitOneIntToken(const char** source, const char* delim,
                      int* value);

bool SplitOneDoubleToken(const char** source, const char* delim,
                         double* value);

void SplitStringIntoKeyValuePairs(const string& line,
                                  const string& key_value_delimiters,
                                  const string& key_value_pair_delimiters,
                                  vector<pair<string, string> >* kv_pairs);

string StringPrintfV(const char* format, va_list ap);

string StringPrintf(const char* format, ...);

void      init_random   ( void );
void      init_random   ( unsigned int ID );
int       random        ( int K);
int*      random_perm   ( int N, int K);


double*   new_VecDoub   ( int len );
int*      new_VecInt    ( int len );
void      del_VecDoub   ( double* v, int len );
void      del_VecInt    ( int* v, int len );
void      print_VecDoub ( double* v, int len );
void      print_VecInt  ( int* v, int len );

double    dot           ( double* a, double* b, int len );
double    norm          ( double* v, int len );
double    distance      ( double* u, double* v, int len);
double    average       ( double* v, int len );
double    normalize     ( double* p, int len );
double    get_entropy   ( double* p, int len );
void      copy          ( double* from, double* to, int len );

int       max_index     ( double* a, int len );

double**  new_MatDoub   ( int row, int col );
int**     new_MatInt    ( int row, int col );
void      del_MatDoub   ( double** m, int row, int col );
void      del_MatInt    ( int** m, int row, int col );
void      print_MatDoub ( double** m, int row, int col );
void      print_MatInt  ( int** m, int row, int col );

void      cholesky      ( double** m, double** L, int row, int col );
void      inverse       ( double** L, double** inv, int row, int col); 
void      solve         ( double** L, double* b, double* x, int len);
void      elsolve       ( double** L, double* b, double* y, int len);
void      elsolve       ( double** L, double** B, double** Y, int len,int col_b);
double    logdet        ( double** L, int row, int col);

vector<string> splitEx  ( string& src, string separate_character );

}
#endif
