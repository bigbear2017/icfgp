#pragma once
#include<fstream>
#include "Util.h"
#define DBL_MIN (1e-999)
using namespace std;

class GP
{
public:
	int N_max_size;
	int N;
	int D;

	double** cov;
	double** L;
	
	double*  hyper;
	double   mean;

	double*  Y;
	double** X;

	double** INV;  // icov Kts run on it
	double** Cd;   // cov_d runs on it
	double** T;    // alpha YY v Kts are running on it

	GP( string path, int _N_max_size, int _D );
	~GP( void );

	void add    ( double* x, double y );
	void remove ( double* x );

	void     getCov     ( double* hyper);
	double** getCov     ( double* hyper, double** Xt, int Nt, int add_noise );
	double** getCov     ( double* hyper, double** Xa, int Na, double** Xb, int Nb );
	void     getCovGrad ( double* hyper, double** cov_d, int dim );

	void     predict_mean         ( double* hyper, double** Xt, double * Yp, int Nt );
	void     predict_variance     ( double* hyper, double** Xt, double* Var, int Nt );
	double   predict_uncertainty  ( double* hyper, double** Xt, int Nt );

	void     evaluate_nlml_grad   ( double* hyper, double& nlml, double* grad );
	void     learn_hyper          ( int max_iter, string log_path );

	void  print_data  ( void );
	void  print_hyper ( void );
};

