#!/bin/sh
mpirun -f hosts -n 4 ./gp_train Tdata/t1/mtr.txt -ft 0 -rr 1 -hp Tdata/t1/mhyp -mp Tdata/t1
mpirun -f hosts -n 4 ./gp_predict -rd Tdata/t1/mtr.txt -ed Tdata/t1/mte.txt -mf Tdata/t1/mn.txt -vf Tdata/t1/vn.txt -mp Tdata/t1
#./gp_train Tdata/t2/mtr1.txt -ft 0 -rr 1 -hp Tdata/t2/mhyp1.txt -mp Tdata/t2/
#./gp_predict -rd Tdata/t2/mtr1.txt -ed Tdata/t2/mte1.txt -mf Tdata/t2/mn.txt -vf Tdata/t2/vn.txt -mp Tdata/t2/
