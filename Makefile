CC=mpicxx

# C-Compiler flags
CFLAGS=-O3 #-Wall

# linker
LD=mpicxx
LFLAGS=-O3 #-Wall

all: gp_train gp_predict train_hyper

clean:
	rm -f *.o
	rm -f gp_train gp_predict train_hyper

io.o: io.cc io.h
	$(CC) -c $(CFLAGS)  io.cc -o io.o

util.o: util.cc util.h
	$(CC) -c $(CFLAGS)  util.cc -o util.o

timer.o: timer.cc timer.h
	$(CC) -c $(CFLAGS)  timer.cc -o timer.o

document.o: document.cc document.h
	$(CC) -c $(CFLAGS)  document.cc -o document.o

cov.o: cov.cc cov.h
	$(CC) -c $(CFLAGS)  cov.cc -o cov.o

model.o: model.cc model.h
	$(CC) -c $(CFLAGS)  model.cc -o model.o

matrix.o: matrix.cc matrix.h
	$(CC) -c $(CFLAGS)  matrix.cc -o matrix.o

matrix_manipulation.o: matrix_manipulation.cc matrix_manipulation.h
	$(CC) -c $(CFLAGS)  matrix_manipulation.cc -o matrix_manipulation.o

parallel_interface.o: parallel_interface.cc parallel_interface.h
	$(CC) -c $(CFLAGS)  parallel_interface.cc -o parallel_interface.o

gp_train.o: gp_train.cc
	$(CC) -c $(CFLAGS)  gp_train.cc -o gp_train.o

gp_predict.o: gp_predict.cc gp_predict.h
	$(CC) -c $(CFLAGS)  gp_predict.cc -o gp_predict.o

train_hyper.o: train_hyper.cc
	$(CC) -c $(CFLAGS) train_hyper.cc -o train_hyper.o

gp_train: timer.o io.o parallel_interface.o util.o model.o document.o matrix.o cov.o matrix_manipulation.o gp_train.o
	$(LD) $(LFLAGS) timer.o io.o parallel_interface.o util.o document.o model.o matrix.o cov.o matrix_manipulation.o gp_train.o -o gp_train

gp_predict: timer.o io.o parallel_interface.o util.o model.o document.o matrix.o cov.o matrix_manipulation.o gp_predict.o
	$(LD) $(LFLAGS) timer.o io.o parallel_interface.o util.o document.o model.o matrix.o cov.o matrix_manipulation.o gp_predict.o -o gp_predict

train_hyper: timer.o io.o parallel_interface.o util.o model.o document.o matrix.o cov.o matrix_manipulation.o train_hyper.o
	$(LD) $(LFLAGS) timer.o io.o parallel_interface.o util.o document.o model.o matrix.o cov.o matrix_manipulation.o train_hyper.o -o train_hyper
