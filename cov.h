#ifndef COV_H__
#define COV_H__
namespace psvm {
	class Sample;
	using std::string;
	class NormalMatrix;
	enum COVType { COVSEISO = 0 };
	class COV
	{
		public:
			virtual double CalcCov(const Sample& a, const Sample& b) const=0;
			virtual std::string get_parm_str() const=0;
			virtual double getSigma() const=0;
			virtual double getMean() const=0;
			//		virtual void set_parm(std::string str) = 0;

		protected:
			// Computes -|a - b|. It is the caller's responsibility to make sure that the features of a and b are sorted according to feature-id in the same
			// order. Otherwise, this method will not return a correct result.
			double OneNormSub(const Sample& a, const Sample& b) const;

			// Computes a.b. It is the caller's responsibility to make sure that the features of a and b are sorted according to feature-id in the same
			// order. Otherwise, this method will not return a correct result.
			double InnerProduct(const Sample& a, const Sample& b) const;

			double TwoNormSub(const Sample& a, const Sample& b) const;
	};

	// Squared Exponential covariance function with isotropic distance measure. The coriance function is k(x^p, k^q) = sf^2 * exp(-(x^p - x^q)' * (x^p - x^q)/(2 * ell^2));
	class CovSEISO : public COV {
		public:
			CovSEISO( const string& file );
			CovSEISO( double sf_, int dim_, double * ell_ ) {
				sf = sf_;
				dim = dim_;
				ell = new double [dim];
				for( int i = 0; i < dim; i++ ) {
					ell[i] = ell_[i];
				}
			}
			virtual double CalcCov(const Sample& a, const Sample& b) const;
			virtual std::string get_parm_str() const;
			virtual ~CovSEISO() {
				delete [] ell;
			}
			double getSigma() const { return sigma; }
			double getMean()  const { return mean;  }
			//		virtual void set_parm(std::string str);
		protected:
			double sf;
			int dim;
			double * ell;
			double sigma; // this variable will not be written into get_param_str.
			double mean;  // this variable will not too.
	};

	//This class is to calculate the covariance matrix
	class CovSENoise: public COV {
		public:
			CovSENoise( double sf_, double nf_, int dim_, double * ell_ ) {
				_sf = sf_;
				_nf = nf_;
				_dim = dim_;
				_ell = new double[ _dim ];
				for( int i = 0; i < _dim; i++ ) {
					_ell[i] = ell_[i];
				}
			}
			virtual double CalcCov( const Sample& a, const Sample& b ) const;
			double getSigma() const { return _nf; }
			double getMean() const { return 0.0; }
			std::string get_parm_str() const { return NULL; }
			virtual ~CovSENoise(){
				delete [] _ell;
			}
		protected:
			double _sf;
			double _nf;
			int _dim;
			double * _ell;
	};
	//This class is to calculate the dirivtive of each hyper paramter SEiso covariance functions
	class CovSEDiv: public CovSEISO {
		public:
			CovSEDiv( double sf, double noise, int dim, double *ell, int index );
			double CalcCov( const Sample & a, const Sample & b ) const;
			virtual ~CovSEDiv(){
			}
		private:
			int _index;
			double _noise;
	};
}
#endif
