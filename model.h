#ifndef MODEL_H__
#define MODEL_H__
#include <vector>
#include "cov.h"
#include "matrix.h"
using namespace std;

namespace psvm {
struct Sample;    // The structure that stores a sample.
class Document;   // The class that encapsulates a dataset.
class ParallelInterface;  // The mpi parallel interface
class LLMatrix;
class ParallelMatrix;

// Stores the training result of Gaussian Process, i.e., the CF of the matrix (I + \sigma^{-2} L^T L). Please refer to the report for detail.
class Model 
{
public:
	Model();
	virtual ~Model();
	
	const COV * get_cov() const { return cov_; }
	void set_cov(COV * c) { cov_ = c; }

	const LLMatrix* get_cf_res() const { return cf_res_; };
	void set_cf_res(LLMatrix * cf)  {
		if(cf_res_ != cf) { 
			delete cf_res_;
			cf_res_ = cf; 
		}
	}

	const ParallelMatrix* get_icf_res() const { return icf_res_; };
	void set_icf_res(ParallelMatrix * icf) {
		if(icf_res_ != icf) {
			delete icf_res_;
			icf_res_ = icf;
		}
	};

	const int get_rank() const { return rank_; };
	void set_rank(int rank) { rank_ = rank; };

	const double get_sigma() const { return sigma_; }
	void set_sigma(double sigma) { sigma_ = sigma; }
	
	const double get_mean() const { return mean_; }
	void set_mean(double mean) { mean_ = mean; }

	const double get_trtime() const { return trtime_; }
	void set_trtime(double trtime) { trtime_ = trtime; }
	
	const double get_tetime() const { return tetime_; }
	void set_tetime(double tetime) { tetime_ = tetime; }
	
	void Save(const char* str_directory, const char* model_name);                // Saves the model to the directory specified by str_directory.
	void SaveHeader(const char* str_directory, const char* model_name);
	void SaveChunks(const char* str_directory, const char* model_name);

	void Load(const char* str_directory, const char* model_name);                // Loads the model from the directory specified by str_directory.
	void LoadHeader(const char* str_directory, const char* model_name);
	void LoadChunks(const char* str_directory, const char* model_name);

private:
	ParallelInterface *mpi_;
	COV * cov_;
	double mean_;
	int rank_;
	double sigma_;
	double trtime_;
	double tetime_ ;
	LLMatrix * cf_res_;         // stores the CF result of matrix (I + \sigma^{-2} L^T L).
	ParallelMatrix * icf_res_;        // stores the ICF result of kernel matrix
};
}
#endif
