#ifndef MATRIX_MANIPULATION_H__
#define MATRIX_MANIPULATION_H__
#include <string>
#include "cov.h"
#include "timer.h"
using namespace std;
namespace psvm {
struct PrimalDualIPMParameter;
class Sample;
class Document;
class LLMatrix;
class NormalMatrix;
class ParallelMatrix;
class COV;

// Provides static methods for manipulating matrix operations, including ICF (Incomplete Cholesky Factorization), CF (Cholesky Factorization), ProductMM (computes HDH'), CholBackwardSub (solves A'x=b) and CholForwardSub (solves Ax=b).
class MatrixManipulation {
 public:
  // Performs Cholesky factorization on 'symmetric_matrix'.
  // The result is a lower triangular matrix 'result', which satisfies
  //           symetric_matrix = result * result'
  //
  // The memory block of 'result' will be allocated inside the function.
  // It is the caller's responsibility to free the memory.
  //
  // If CF is successfully done, return true; otherwise, return false.
  static bool CF(const LLMatrix &symmetric_matrix, LLMatrix *result);

  // Performs incomplete Cholesky factorization incrementally on the kernel
  // matrix, and the resulting matrix is stored distributedly.
  //
  // ICF performs a incremental version of incomplete cholesky factorization.
  //   Firstly, original ICF matrix is loaded from directory
  // 'parameter.pc_path'. If no such matrix avaiable, we regard a 0*0 matrix
  // is loaded.
  //   After the original matrix is loaded (assume a n-by-p matrix), an
  // incremental incomplete cholesky factorization is performed, which expands
  // the original n-by-p matrix to a (n+n_plus)-by-(p+p_plus) matrix. n_plus
  // is the number of added samples provided by 'doc', p_plus is the number of
  // columns need to be attached to the original matrix. If n and p are zeros,
  // an ordinary incomplete cholesky factorization is performed.
  //
  // Kernel matrix is not actually formed and stored in memory. When a
  // matrix element is required, it is calculated based on corresponding
  // samples (provided by 'doc', or broadcasted from other machines),
  // kernel parameters ('kernel').
  //
  // The resulting ICF matrix is stored in 'icf', which is distributed by rows.
  // The memory blocks of 'icf' is allocated in this function, it's the caller's
  // obligation to free these memory blocks, which can be done in the
  // destructor of class ParallelMatrix.
  static void ICF(const Document &doc, const COV &cov, int rows, int columns, double threshold, ParallelMatrix *icf);


  static void ICF(const Document &doc, const COV &cov, int rows, int columns, double threshold, ParallelMatrix *icf, int * pivot);

  static void CF( const Document &doc, const COV &cov, int row, int columns, double threshold, ParallelMatrix * cf );
  // Computes I + h'*d*h where d is a diagonal matrix, h is parallel matrix and rows are distributed among machines, so reductions between computers are necessary. 
  // The result is a symmetric matrix stored on machine ID:0 and other computers do not have this result. l's space will be allocated inside the function.
  static void ProductMM(const ParallelMatrix &icf, const double *diagonal, LLMatrix *product);

// Computes I + h'*d*h where d is a diagonal matrix, h is parallel matrix and rows are distributed among machines, so reductions between computers are necessary. 
  // The result is a symmetric matrix stored on all machines. l's space will be allocated inside the function.
  static void ProductAllMM( const ParallelMatrix &icf, const double *diagonal, LLMatrix *product);

  // Resolves a'x = b where a is lower triangular matrix, x and b are vectors. It is easy to be solved by backward substitutions.
  static void CholBackwardSub(const LLMatrix &a, const double *b, double *x);

  static void CholBackwardSub(const ParallelMatrix &cf, const double * b, double * x );

  static void CholBackwardSubT( const ParallelMatrix& a, const double * b, double * x );

  // Resolves a'x = b where a is lower triangular matrix, x and b are matrices. It is easy to be solved by backward substitutions.
  static void CholBackwardSub(const LLMatrix &a, const NormalMatrix *b, NormalMatrix *x);


  static void CholBackwardSubT( const ParallelMatrix& a, NormalMatrix &b, NormalMatrix * x );

  // Resolves ax = b where a is lower triangular matrix, x and b are vectors. It is easy to be solved by forward substitutions.
  static void CholForwardSub(const LLMatrix &a, const double *b, double *x);

  static void CholForwardSub(const ParallelMatrix &cf, const double * b, double * x, const int * pivot);

  static void CholForwardSub(const ParallelMatrix &cf, const double * b, double * x );

  // Resolves ax=b where a is lower triangular matrix, x and b are normal matrices. It is easy to be solved by forward substitutions.
  static void CholForwardSub(const LLMatrix &a, const NormalMatrix *b, NormalMatrix *x);

  static void CholForwardSub( const ParallelMatrix & a, NormalMatrix &b, NormalMatrix * x );


  // Compute product = a' * b, where a and b are both parallel matrices while product is a normal matrix. 
  static void PlPlProduct(const ParallelMatrix &a, const ParallelMatrix &b, NormalMatrix *product);

  // Compute product = a*b, where a is a parallel matrix, b is a normal matrix, and product is also a parallel matrix.
  static void PlNmProduct(const ParallelMatrix &a, const NormalMatrix &b, ParallelMatrix *product);

  //Compute c =  a' * b, where a is parallel matrix, b is a normal matrix, and c is also matrix on machine
  //ID:0
  static void TplNmProduct( const ParallelMatrix &a, const NormalMatrix &b, NormalMatrix * c );


  //Compute c = a * b' where a is normal matrix and b is a parallel matrix, and c is normal matrix
  static void NmTplProduct(  const NormalMatrix &a, const ParallelMatrix &b, NormalMatrix * c ); 

  static void PlNmProduct( const ParallelMatrix &a, const NormalMatrix &b, NormalMatrix * c );

  static void NmNmProduct( const NormalMatrix &a, const NormalMatrix &b, NormalMatrix * c );

  static void NmNmtProduct( const NormalMatrix &a, const NormalMatrix &b, NormalMatrix * c );

  static double ParallelLogDet( const ParallelMatrix & pm, const int * pivot );
  
  static double ParallelLogDet( const ParallelMatrix & pm );

  static void LLMatrixInverse( const LLMatrix & m, NormalMatrix * r );

  static void ParallelTranspose( const ParallelMatrix & a, ParallelMatrix * b );
  
  static void ParallelTranspose( const ParallelMatrix & a, ParallelMatrix * b, const int *pivot );

  static void ParallelLLTranspose( const ParallelMatrix & a, ParallelMatrix * b );

  static void PmPmTrace( const ParallelMatrix & a, const ParallelMatrix & b, double & trace );
 private:
  // Broadcasts the pivot sample from local machine to other machines. This function is used in ICF.
  static void BroadcastPivotSample(const Document &doc, int pivotSampleGlobalID, Sample **pivotSample);

  // Frees pivot sample memory space (pointed by pPivotSample). The pivot index is also provided to verify whether the current processor
  // needs to free the pivot sample. This function is used in ICF.
  static void FreePivotSample(Sample *pPivotSample, int pivotIndex);

};
}  // namespace psvm

#endif
